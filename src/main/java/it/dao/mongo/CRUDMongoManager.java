package it.dao.mongo;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.*;
import com.mongodb.client.model.Field;
import com.mongodb.client.model.Indexes;
import com.mongodb.client.model.UnwindOptions;
import it.common.chain.IScope;
import it.common.interfa.CRUDManager;
import it.common.interfa.Client;
import it.common.model.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.*;

import static com.mongodb.client.model.Accumulators.*;
import static com.mongodb.client.model.Aggregates.*;
import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Projections.computed;
import static com.mongodb.client.model.Sorts.ascending;
import static com.mongodb.client.model.Sorts.descending;

public class CRUDMongoManager implements CRUDManager {
    private static final Logger LOGGER = LogManager.getLogger(CRUDMongoManager.class);
    private static MongoClient mongoClient;
    private static String DBNAME;
    private static final String CUSTOMERTABLE = "user";
    private static final String ARTISTSTABLE = "artists";
    private static final String SONGSTABLE = "songs";
    private static final String NAME = "name";
    private static final String GENREALBUM = "albums.genre";
    private static final String ALBUMSONG = "albums.songs.name";
    private static final String FIRSTNAME = "firstName";
    private static final String LASTNAME = "lastName";
    private static final String LOGINUSER = "login.user";
    private static final String LOGINPASS = "login.password";
    private static final String ID = "_id";

    public void setUpManager(IScope scope) {
        try {
            MongoClientURI uri = new MongoClientURI((String) scope.getParameter(IScope.DBCONN));
            mongoClient = new MongoClient(uri);
            DBNAME = (String) scope.getParameter(IScope.DBNAME);
        } catch (Exception e) {
            LOGGER.error(e);
        }
    }

    public MongoClient getMongoClient() {
        return mongoClient;
    }

    public void exitManager() {
        try {
            mongoClient.close();
        } catch (Exception e) {
            LOGGER.error(e);
        }

    }

    public Object initDB() {
        MongoCollection<Document> collection;
        Document document;
        try {

            collection = getmongoDatabase().getCollection(CUSTOMERTABLE);
            document = new Document(LOGINUSER, 1).append(LOGINPASS, 1).append(ID, 1); //index for find when user will log
            collection.createIndex(document);


            collection = getmongoDatabase().getCollection(SONGSTABLE);
            document = new Document("genre", 1).append(ID, 1);
            collection.createIndex(document);

            document = new Document("date", 1).append(ID, 1);
            collection.createIndex(document);

            document = new Document("artistkey", 1).append(ID, 1);
            collection.createIndex(document);
        } catch (Exception e) {
            LOGGER.error(e);
        }

        try {
            collection = getmongoDatabase().getCollection(SONGSTABLE);
            document = new Document(ID, 1).append("lyrics", "text");
            collection.createIndex(document);

            document = new Document("name", 1).append(ID, 1);
            collection.createIndex(document);
        } catch (Exception e) {
            LOGGER.error(e);
        }

        try {
            collection = getmongoDatabase().getCollection(ARTISTSTABLE);
            document = new Document(GENREALBUM, 1).append("_id", 1);
            collection.createIndex(document);
        } catch (Exception e) {
            LOGGER.error(e);
        }


        return true;
    }

    @Override
    public Object read(Object o) {
        Document result;
        MongoCollection<Document> collection;
        FindIterable<Document> documents = null;
        Map map = new HashMap<>();
        if (o instanceof Customer) {
            try {
                collection = getmongoDatabase().getCollection(CUSTOMERTABLE);
                if (((Customer) o).getLogin() != null) {
                    Login login = new Login();
                    login.setUser(((Customer) o).getLogin().getUser());
                    if (((Customer) o).getLogin().getPassword() != null) {
                        login.setPassword(((Customer) o).getLogin().getPassword());
                        documents = collection.find(and(eq(LOGINUSER, login.getUser()), eq(LOGINPASS, login.getPassword())));
                    } else {
                        documents = collection.find(new Document().append(LOGINUSER, login.getUser()));
                    }

                    result = documents.first();
                } else {
                    if (((Customer) o).getFirstName() != null) {
                        if (((Customer) o).getLastName() != null) {
                            documents = collection.find(new Document().append(FIRSTNAME, ((Customer) o).getFirstName())
                                    .append(LASTNAME, ((Customer) o).getLastName()));
                        } else {
                            documents = collection.find(new Document().append(FIRSTNAME, ((Customer) o).getFirstName()));
                        }
                    } else if (((Customer) o).getLastName() != null) {
                        documents = collection.find(new Document().append(LASTNAME, ((Customer) o).getLastName()));
                    }
                    result = Objects.requireNonNull(documents).first(); //only to check if it find something
                    if (result != null) {
                        MongoCursor<Document> iterator = documents.iterator();
                        if (iterator.hasNext()) {
                            Customer temp;
                            while (iterator.hasNext()) {
                                result = iterator.next();
                                temp = new Gson().fromJson(result.toJson(), Customer.class);
                                temp.setCustId(result.getObjectId(ID));
                                map.put(temp.getCustId(), temp);
                            }
                            return map;
                        }
                    }
                }
            } catch (Exception e) {
                LOGGER.error(e);
                return null;
            }
            if (result != null) {
                Customer c = new Gson().fromJson(result.toJson(), Customer.class);
                c.setCustId(result.getObjectId(ID));
                return c;
            }
            return map;

        } else if (o instanceof Artist) {
            try {
                collection = getmongoDatabase().getCollection(ARTISTSTABLE);
                if (((Artist)o).getId()!=null){
                    documents = collection.find(new Document().append(ID, ((Artist) o).getId()));
                }else {
                    documents = collection.find(new Document().append(NAME, ((Artist) o).getName()));
                }
                result = documents.first(); //only to check if it find something
            } catch (Exception e) {
                LOGGER.error(e);
                return null;
            }
            if (result != null) {
                Artist a = new Gson().fromJson(result.toJson(), Artist.class);
                a.setId(result.getObjectId(ID));
                for (Album album : a.getAlbums()){
                    album.setArtist(a);
                    album.setArtistOnSongs(a.getId());
                    album.addIdOnSongs();
                }
                return a;
            }
            return null;
        } else if (o instanceof Song) {
            try {
                collection = getmongoDatabase().getCollection(SONGSTABLE);
                documents = collection.find(new Document().append(NAME, ((Song) o).getName()));
                result = documents.first();
            } catch (Exception e) {
                LOGGER.error(e);
                return null;
            }
            if (result != null) {
                MongoCursor<Document> iterator = documents.iterator();
                if (iterator.hasNext()) {
                    Song temp;
                    while (iterator.hasNext()) {
                        result = iterator.next();
                        temp = new Gson().fromJson(result.toJson(), Song.class);
                        temp.setId(result.getObjectId(ID));
                        map.put(temp.getName(), temp);
                    }
                    return map;
                }
            }
            return map;
        }

        return map;
    }

    @Override
    public boolean write(Object o) {
        if (o instanceof Artist) {
            try {
                MongoCollection<Document> collection = getmongoDatabase().getCollection(ARTISTSTABLE);
                Document document = Document.parse(getJson(o));
                collection.insertOne(document);
                ((Artist) o).setId(document.getObjectId(ID));
                for (Album album : ((Artist) o).getAlbums()) {
                    album.setArtistOnSongs(((Artist) o).getId());
                    write(album.getSongs());
                }
                LOGGER.info("DOC SAVED");
            } catch (Exception e) {
                LOGGER.error(e);
                return false;
            }

        } else if (o instanceof Song) {
            try {
                MongoCollection<Document> collection = getmongoDatabase().getCollection(SONGSTABLE);
                Document document = Document.parse(getJson(o));
                collection.insertOne(document);
                ((Song) o).setId(document.getObjectId(ID));
            } catch (Exception e) {
                LOGGER.error(e);
                return false;
            }
        } else if (o instanceof Client) {
            try {
                MongoCollection<Document> collection = getmongoDatabase().getCollection(CUSTOMERTABLE);
                Document document = Document.parse(getJson(o));
                collection.insertOne(document);
                ((Client) o).setCustId(document.getObjectId(ID));
                LOGGER.info("DOC SAVED");
            } catch (Exception e) {
                LOGGER.error(e);
                return false;
            }
        } else if (o instanceof List) {
            List list = ((List) o);
            try {
                if (list.get(0) instanceof Song) {
                    List<Song> songs = (List<Song>) o;
                    MongoCollection<Document> collection = getmongoDatabase().getCollection(SONGSTABLE);
                    List<Document> jsonList = new ArrayList<>();
                    for (Song a : songs) {
                        Document jsnObject = Document.parse(getJson(a));
                        jsonList.add(jsnObject);
                    }
                    collection.insertMany(jsonList);
                } else if (list.get(0) instanceof Artist) {
                    List<Artist> artists = (List<Artist>) o;
                    MongoCollection<Document> collection = getmongoDatabase().getCollection(ARTISTSTABLE);
                    List<Document> jsonList = new ArrayList<>();
                    Album album;
                    for (Artist a : artists) {
                        Document jsnObject = Document.parse(getJson(a));
                        jsonList.add(jsnObject);
                    }
                    collection.insertMany(jsonList);
                    for (Document document : jsonList) {
                        if (document.get("albums") != null) {
                            List<Document> albums = (List<Document>) document.get("albums");
                            for (Document doc : albums) {
                                album = new Gson().fromJson(doc.toJson(), Album.class);
                                album.setArtistOnSongs(document.getObjectId(ID));
                                write(album.getSongs());
                            }
                        }
                    }
                }

            } catch (Exception e) {
                LOGGER.error(e);
                return false;
            }

        }

        return true;
    }

    @Override
    public Object update(Object o) {
        MongoCollection<Document> collection;

        Document document = Document.parse(getJson(o));

        if (o instanceof Customer) {
            try {
                List<Song> songs = ((Customer)o).getPrefSong();
                if (songs!=null){
                    document.put("prefSong",setIdSongs(document,songs));
                }
                collection = getmongoDatabase().getCollection(CUSTOMERTABLE);
                document.put(ID,((Customer)o).getCustId());
                collection.findOneAndReplace(eq(ID, ((Customer) o).getCustId()), document);
                LOGGER.info("YOU UPDATE" + ((Customer)o).getFirstName().concat(" "+((Customer)o).getLastName()));
                return true;
            } catch (Exception e) {
                LOGGER.error(e);
            }
        } else if (o instanceof Song) {
            collection = getmongoDatabase().getCollection(SONGSTABLE);
            document.put(ID,((Song)o).getId());
            collection.replaceOne(eq(ID, ((Song) o).getId()), document);
            Artist artist = new Artist();
            artist.setId(new ObjectId(((Song) o).getArtistkey()));
            artist = (Artist) read(artist);
            artist.updateSong((Song) o);

            collection = getmongoDatabase().getCollection(ARTISTSTABLE);
            document = Document.parse(getJson(artist));
            document.put(ID,artist.getId());
            collection.replaceOne(eq(ID, artist.getId()), document);
            LOGGER.info("YOU UPDATE" );
            return true;

        } else if (o instanceof Artist) {
            collection = getmongoDatabase().getCollection(ARTISTSTABLE);
            document.put(ID,((Artist)o).getId());
            collection.replaceOne(eq(ID, ((Artist) o).getId()), document);

            LOGGER.info("YOU UPDATE" );
            return true;
        }
        return false;
    }

    private List<Document> setIdSongs(Document document,List<Song> songs){
        List<Document> documents = (List<Document>) document.get("prefSong");
        for (Document document1: documents){
            for (Song song: songs){
                if ((document1.get("name")).equals(song.getName())){
                    document1.put("_id",song.getId());
                }
            }

        }
        return documents;
    }
    @Override
    @Deprecated
    public Object delete(Object o, String id) {
        return false;
    }

    public Object delete(Object o) {
        MongoCollection<Document> collection;
        try {
            if (o instanceof Customer) {
                collection = getmongoDatabase().getCollection(CUSTOMERTABLE);
                collection.deleteOne(eq(ID, ((Customer) o).getCustId()));
                LOGGER.info("YOU DELETE" + o);
                return true;
            } else if (o instanceof Song) {
                collection = getmongoDatabase().getCollection(SONGSTABLE);
                collection.deleteOne(eq(ID, ((Song) o).getId()));

                Artist artist = new Artist();
                artist.setId(new ObjectId(((Song) o).getArtistkey()));
                artist = (Artist) read(artist);
                artist.removeSong((Song) o);

                collection = getmongoDatabase().getCollection(ARTISTSTABLE);
                Document document = Document.parse(getJson(artist));
                document.put(ID,artist.getId());
                collection.replaceOne(eq(ID, artist.getId()), document);
                LOGGER.info("YOU DELETE" + o);
                return true;

            } else if (o instanceof Artist) {
                collection = getmongoDatabase().getCollection(ARTISTSTABLE);
                collection.deleteOne(eq(ID, ((Artist) o).getId()));

                collection = getmongoDatabase().getCollection(SONGSTABLE);
                collection.deleteMany(eq("artistkey", ((Artist) o).getId().toString()));
                LOGGER.info("YOU DELETE" + o);
                return true;
            }
        } catch (Exception e) {
            LOGGER.error(e);
        }

        return false;
    }

    public boolean defineIndex() {
        MongoCollection<Document> collection = getmongoDatabase().getCollection(SONGSTABLE);
        collection.createIndex(Indexes.text("lyrics"));
        for (Document index : collection.listIndexes()) {
            System.out.println(index.toJson());
        }
        return false;
    }

    public Map getMostFavoriteSong() {
        Map<String, Long> map = new LinkedHashMap<>();
        MongoCollection<Document> collection = getmongoDatabase().getCollection(CUSTOMERTABLE);
        AggregateIterable<Document> output = collection.aggregate(Arrays.asList(unwind("$prefSong"), group("$prefSong.name", sum("count", 1L)), sort(descending("count")), limit((int) 20L)));

        for (Document dbObject : output) {
            map.put(dbObject.getString(ID), dbObject.getLong("count"));
            LOGGER.info("element extract is " + dbObject);

        }
        return map;
    }

    public Map getMostFavoriteSongForGenre(String genre) {
        Map<String, Long> map = new LinkedHashMap<>();
        MongoCollection<Document> collection = getmongoDatabase().getCollection(CUSTOMERTABLE);
        AggregateIterable<Document> output = collection.aggregate(Arrays.asList(unwind("$prefSong",
                new UnwindOptions().includeArrayIndex("song")), group("$prefSong.genre", push("songs", "$prefSong")), match(eq(ID, genre)), unwind("$songs"), group("$songs.name", sum("count", 1L)), sort(descending("count")), limit((int) 20L)));

        for (Document dbObject : output) {
            map.put(dbObject.getString(ID), dbObject.getLong("count"));
            LOGGER.info("element extract is " + dbObject);

        }
        return map;
    }

    public Map getMostFavoriteSongForArtist(Artist artist) {
        Map<String, Long> map = new LinkedHashMap<>();
        MongoCollection<Document> collection = getmongoDatabase().getCollection(CUSTOMERTABLE);
        AggregateIterable<Document> output = collection.aggregate(Arrays.asList(unwind("$prefSong",
                new UnwindOptions().includeArrayIndex("song")), group("$prefSong.artistkey", push("songs", "$prefSong")), match(eq(ID, artist.getId().toString())), unwind("$songs"), group("$songs.name", sum("count", 1L)), sort(descending("count")), limit((int) 20L)));

        for (Document dbObject : output) {
            map.put(dbObject.getString(ID), dbObject.getLong("count"));
            LOGGER.info("element extract is " + dbObject);
        }

        return map;
    }

    public Map getMeanWordsForSongPerYear() {
        Map<String, Double> map = new LinkedHashMap<>();
        MongoCollection<Document> collection = getmongoDatabase().getCollection(SONGSTABLE);
        AggregateIterable<Document> output = collection.aggregate(Arrays.asList(new Document("$project",
                        new Document("words",
                                new Document("$split", Arrays.asList("$lyrics", " ")))
                                .append("year", "$date")
                                .append("name", "$name")),
                new Document("$unwind",
                        new Document("path", "$words")),
                new Document("$group",
                        new Document("_id",
                                new Document("id", "$_id")
                                        .append("name", "$name")
                                        .append("year", "$year"))
                                .append("numWord",
                                        new Document("$sum", 1L))),
                new Document("$group",
                        new Document("_id", "$_id.year")
                                .append("numsongforYear",
                                        new Document("$sum", 1L))
                                .append("numTotWordForYear",
                                        new Document("$sum", "$numWord"))),
                new Document("$group",
                        new Document("_id", "$_id")
                                .append("avgWordsForYears",
                                        new Document("$avg",
                                                new Document("$divide", Arrays.asList("$numTotWordForYear", "$numsongforYear"))))),
                new Document("$sort",
                        new Document("avgWordsForYears", -1L)),
                new Document("$limit", 20L)));

        for (Document dbObject : output) {
            map.put(dbObject.getString(ID), dbObject.getDouble("avgWordsForYears"));
            LOGGER.info("element extract is " + dbObject);

        }
        return map;
    }

    public Map getMeanSongsForArtistPerYear() {
        Map<String, Double> map = new LinkedHashMap<>();
        MongoCollection<Document> collection = getmongoDatabase().getCollection(SONGSTABLE);
        AggregateIterable<Document> output = collection.aggregate(Arrays.asList(new Document("$group",
                        new Document("_id", "$date")
                                .append("numSongs",
                                        new Document("$sum", 1L))
                                .append("artists",
                                        new Document("$addToSet", "$artistkey"))),
                new Document("$addFields",
                        new Document("numArtists",
                                new Document("$size", "$artists"))),
                new Document("$project",
                        new Document("_id", "$_id")
                                .append("avgSongsForArtistPerYears",
                                        new Document("$cond", Arrays.asList(new Document("$or", Arrays.asList(new Document("$eq", Arrays.asList("$numArtists", 0L)),
                                                new Document("$eq", Arrays.asList("$numSongs", 0L)))), 0L,
                                                new Document("$avg",
                                                        new Document("$divide", Arrays.asList("$numSongs", "$numArtists"))))))),
                new Document("$sort",
                        new Document("avgSongsForArtistPerYears", -1L)),
                new Document("$limit", 20L)));

        for (Document dbObject : output) {
            if (dbObject.getString(ID)!=null)
                map.put(dbObject.getString(ID), dbObject.getDouble("avgSongsForArtistPerYears"));
            LOGGER.info("element extract is " + dbObject);
        }
        return map;
    }

    public Map getMostPreferGenre() {
        Map<String, Long> map = new LinkedHashMap<>();
        MongoCollection<Document> collection = getmongoDatabase().getCollection(CUSTOMERTABLE);
        AggregateIterable<Document> output = collection.aggregate(Arrays.asList(unwind("$prefSong",
                new UnwindOptions().includeArrayIndex("song")), group("$prefSong.genre", push("songs",
                "$prefSong"), sum("NumSongsForGenre", 1L)),
                sort(descending("NumSongsForGenre")), limit((int) 20L)));

        for (Document dbObject : output) {
            map.put(dbObject.getString(ID), dbObject.getLong("NumSongsForGenre"));
            LOGGER.info("element extract is " + dbObject);
        }
        return map;
    }

    public Map getMostUsedWordsForArtist(Artist artist) {
        Map<String, Long> map = new LinkedHashMap<>();
        MongoCollection<Document> collection = getmongoDatabase().getCollection(ARTISTSTABLE);
        AggregateIterable<Document> output = collection.aggregate(Arrays.asList(match(eq(ID,
                artist.getId())), group("$name", push("songs", "$albums.songs")), unwind("$songs",
                new UnwindOptions().preserveNullAndEmptyArrays(true)), unwind("$songs",
                new UnwindOptions().preserveNullAndEmptyArrays(true)), unwind("$songs",
                new UnwindOptions().preserveNullAndEmptyArrays(true)), project(computed("words",
                eq("$split", Arrays.asList("$songs.lyrics", " ")))), unwind("$words",
                new UnwindOptions().preserveNullAndEmptyArrays(true)), addFields(new Field("leght",
                        new Document("$strLenCP", "$words"))), match(gt("leght", 3L)),
                group("$words", sum("count", 1L)), sort(descending("count"))
                , limit((int) 20L)));

        for (Document dbObject : output) {
            map.put(dbObject.getString(ID), dbObject.getLong("count"));
            LOGGER.info("element extract is " + dbObject);
        }
        return map;
    }

    public Map getMostUsedWordsForGenre(String genre) {
        Map<String, Long> map = new LinkedHashMap<>();
        MongoCollection<Document> collection = getmongoDatabase().getCollection(SONGSTABLE);
        AggregateIterable<Document> output = collection.aggregate(Arrays.asList(new Document("$project",
                        new Document("words",
                                new Document("$split", Arrays.asList("$lyrics", " ")))
                                .append("genre", "$genre")),
                new Document("$unwind",
                        new Document("path", "$words")),
                new Document("$addFields",
                        new Document("leght",
                                new Document("$strLenCP", "$words"))),
                new Document("$match",
                        new Document("leght",
                                new Document("$gt", 3L))
                                .append("genre", genre)),
                new Document("$group",
                        new Document(ID, "$words")
                                .append("count",
                                        new Document("$sum", 1L))),
                new Document("$sort",
                        new Document("count", -1L)),
                new Document("$limit", 20L)));

        for (Document dbObject : output) {
            map.put(dbObject.getString(ID), dbObject.getLong("count"));
            LOGGER.info("element extract is " + dbObject);
        }
        return map;
    }

    public Map getArtistMostPref() {
        Map<String, Long> map = new LinkedHashMap<>();
        MongoCollection<Document> collection = getmongoDatabase().getCollection(CUSTOMERTABLE);
        AggregateIterable<Document> output = collection.aggregate(Arrays.asList(new Document("$unwind",
                        new Document("path", "$prefSong")
                                .append("includeArrayIndex", "song")),
                new Document("$project",
                        new Document("songs", "$prefSong")
                                .append("artist", "$prefSong.artistkey")),
                new Document("$group",
                        new Document(ID,
                                new Document("$toObjectId", "$artist"))
                                .append("songs",
                                        new Document("$push", "$songs"))
                                .append("NumSongsForGenre",
                                        new Document("$sum", 1L))),
                new Document("$limit", 20L),
                new Document("$lookup",
                        new Document("from", "artists")
                                .append("localField", ID)
                                .append("foreignField", ID)
                                .append("as", "artist")),
                new Document("$unwind",
                        new Document("path", "$artist")
                                .append("preserveNullAndEmptyArrays", false)),
                new Document("$sort",
                        new Document("NumSongsForGenre", -1L)),
                new Document("$limit", 20L),
                new Document("$project",
                        new Document(ID, "$artist.name")
                                .append("songs", "$songs")
                                .append("count", "$NumSongsForGenre"))));

        for (Document dbObject : output) {
            map.put(dbObject.getString(ID), dbObject.getLong("count"));
            LOGGER.info("element extract is " + dbObject);

        }
        return map;
    }

    public List<Song> getRandomsSong(int i, Customer customer) {
        List<Song> songs = new ArrayList<>();
        Document result = null;
        MongoCollection<Document> collection;
        FindIterable<Document> documents = null;

        try {
            collection = getmongoDatabase().getCollection(ARTISTSTABLE);
            Document document = new Document();
            if (customer.getPrefGen()!=null){
                document.append(GENREALBUM, customer.getPrefGen());
            }
            long l = collection.countDocuments(document);
            if (l == 0) {
                l = 10;
                document = new Document();
            }
            long skip = Math.round((double) l / i);
            int offset = (int) ((skip * l + (int) ((Math.random() * skip) % l)) % l);
            offset = offset - 5;
            if (customer.getPrefGen() != null) {
                documents = collection.find(document).skip(offset).limit(i);
            }else {
                documents = collection.find().skip(offset).limit(i);
            }

            result = documents.first();

            if (result != null) {
                MongoCursor<Document> iterator = documents.iterator();
                if (iterator.hasNext()) {
                    Artist temp;
                    Document tempId;
                    Map tempMap;
                    Song s;
                    while (iterator.hasNext()) {
                        tempId = iterator.next();
                        temp = new Gson().fromJson(tempId.toJson(), Artist.class);
                        temp.setId(tempId.getObjectId(ID));
                        temp = insertSongsOnArtist(temp);
                        int numSong = temp.getSongs().size();
                        int j = new Random().nextInt(numSong);
                        Song tempSong = temp.getSongs().get(j);
                        tempMap = (Map)read(tempSong);
                        s = (Song) tempMap.get(tempSong.getName());
                        ObjectId id = s.getId();
                        if (id!=null){
                            tempSong.setId(id);
                            songs.add(tempSong);
                        }
                    }
                    if (songs.size()<i){
                        Customer tempCust = new Customer();
                        tempCust.setPrefGen(null);
                        return getRandomsSong(i,tempCust);
                    }
                    else
                        return songs;
                }
            }
        } catch (Exception e) {
            LOGGER.error(e);
            return null;
        }
        return null;
    }

    private Artist insertSongsOnArtist(Artist a){
        for (Album album : a.getAlbums()) {
            album.setArtist(a);
            for (Song song : album.getSongs()) {
                song.setArtist(a);
                song.setAlbum(album);
                song.setArtistkey(a.getId().toString());
                a.addSong(song);
            }
        }
        return a;
    }

    public String getJson(Object o) {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        return gson.toJson(o);
    }

    private MongoDatabase getmongoDatabase() {
        return mongoClient.getDatabase(DBNAME);
    }
}
