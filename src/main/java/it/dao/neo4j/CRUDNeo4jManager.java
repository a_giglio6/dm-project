package it.dao.neo4j;

import com.google.gson.GsonBuilder;
import it.common.interfa.CRUDManager;
import it.common.model.Album;
import it.common.model.Artist;
import it.common.model.Song;
import it.dao.CRUDGenericManager;
import org.bson.types.ObjectId;
import org.neo4j.driver.v1.*;
import com.google.gson.Gson;
import static org.neo4j.driver.v1.Values.parameters;

import it.common.chain.IScope;
import it.common.model.Customer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CRUDNeo4jManager implements CRUDManager {

    private static final Logger LOGGER = LogManager.getLogger(CRUDNeo4jManager.class);

    private Driver driver;


    public void close() {
        try {
            driver.close();
        }
        catch (Exception e) {
            LOGGER.error(e);
        }
    }

    public void setUpManager(IScope scope){
        try {
            driver = GraphDatabase.driver((String)scope.getParameter(IScope.NEOCONN), AuthTokens.basic((String)scope.getParameter(IScope.NEOUSER),(String)scope.getParameter(IScope.NEOPASS)));
        } catch (Exception e) {
            LOGGER.error(e);
        }
    }
    @Override
    public boolean write(Object o) {
        int size= ((Customer) o).getPrefSong().size();
        Song s = ((Customer) o).getPrefSong().get(size-1);
        Album a = null;

        a = getAlbum(s);


        a.setArtist(null);
        a.setSongs(new ArrayList<>());

        s.setAlbum(a);
        s.setArtist(null);
        String album = a.getName();
        String json = getJson(s);

        String json2 = getJson(a);

        if (o instanceof Customer) {
            try(Session session = driver.session()){
                session.writeTransaction((TransactionWork<Void>) tx -> {
                    tx.run("MERGE (c:Customer {name: $name, surname: $surname, id: $custId})", parameters("name", ((Customer) o).getFirstName(),"surname", ((Customer) o).getLastName(), "custId", ((Customer) o).getCustId().toString()));
                    return null;
                });
                session.writeTransaction((TransactionWork<Void>) tx -> {
                    String song= ((Customer) o).getPrefSong().get(size-1).getName();
                    tx.run("MERGE (s:Song {name: $songname, id: $songId, obj: $obj})", parameters("songname", song, "songId", s.getId().toString(), "obj", json));
                    return null;
                });
                session.writeTransaction((TransactionWork<Void>) tx -> {

                    tx.run("MERGE (a:Album {name: $albumname,obj: $obj})", parameters("albumname", album, "obj", json2));
                    return null;
                });
                session.writeTransaction(((TransactionWork<Void>) tx -> {
                    String song= ((Customer) o).getPrefSong().get(size-1).getId().toString();
                    tx.run("MATCH (c:Customer  {id: $id_cust}) "+
                            "MATCH (s:Song {id: $songid}) "+
                            "MERGE (c)-[:Likes]->(s)", parameters("id_cust", ((Customer) o).getCustId().toString(), "songid", song));
                    return null;
                }));
                session.writeTransaction(((TransactionWork<Void>) tx -> {
                    String song= ((Customer) o).getPrefSong().get(size-1).getId().toString();
                    tx.run("MATCH (s:Song  {id: $id_s}) "+
                            "MATCH (a:Album {name: $albumname}) "+
                            "MERGE (s)-[:BelongTo]->(a)", parameters("id_s", song, "albumname", album));
                    return null;
                }));
                return true;
            } catch (Exception e) {
                LOGGER.error(e);
                return false;
            }
        }
        else

            return false;
    }

    @Override
    public Map<String, List<String>> read(Object o) {
        Map<String, List<String>> song_alb = new HashMap<String, List<String>>();
        if ( o instanceof Customer) {
            int size = ((Customer) o).getPrefSong().size();
            try (Session session = driver.session()) {
                session.readTransaction(new TransactionWork<Map<String, List<String>>>() {
                    @Override
                    public Map<String, List<String>> execute(Transaction tx) {
                        for (int i = 0; i < size; i++) {
                            StatementResult result = tx.run("MATCH (c: Customer )-[:Likes]->(s: Song )<-[:Likes]-(a: Customer)-[Likes]->(x: Song)-[:BelongTo]->(y: Album) " +
                                    "WHERE c.name= $name " +
                                    "AND c.surname= $surname " +
                                    "AND s.name= $name_s " +
                                    "RETURN {SONGOBJ: x.obj, SONG: x.name, ALBUMOBJ: y.obj}",
                                    parameters("name", ((Customer) o).getFirstName(),"surname", ((Customer) o).getLastName(), "name_s", ((Customer) o).getPrefSong().get(i).getName()));

                            while (result.hasNext()) {
                                 Record s_a = result.next();
                                 List<String> supp = new ArrayList<>();
                                 supp.add(s_a.get(0).get("SONG").asString());
                                 supp.add(s_a.get(0).get("ALBUMOBJ").asString());
                                 song_alb.put(s_a.get(0).get("SONGOBJ").asString(), supp);
                            }
                        }
                        return song_alb;
                    }
                });
            } catch (Exception e) {
                LOGGER.error(e);
                return null;
            }
        }
        return song_alb;
    }


    @Override
    public Object update(Object o) {
        if (o instanceof Customer) {
            try (Session session = driver.session()) {
                session.writeTransaction((TransactionWork<Void>) tx -> {
                    tx.run("MATCH (c:Customer {id: $custid }) " +
                            "SET c.name = $name, c.surname = $surname RETURN c", parameters("name", ((Customer) o).getFirstName(), "surname", ((Customer) o).getLastName(), "custid", ((Customer) o).getCustId().toString()));
                    return null;
                });
            } catch (Exception e) {
                LOGGER.error(e);
                return false;
            }
        } else if (o instanceof Song){
            String json3 = getJson(o);
            try (Session session = driver.session()) {
                session.writeTransaction((TransactionWork<Void>) tx -> {
                    tx.run("MATCH (s: Song {id: $songId}) " +
                            "SET s+= {name: $name, obj: $obj}", parameters("songId", ((Song) o).getId().toString(), "name", ((Song) o).getName(), "obj", json3));
                    return null;
                });
            } catch (Exception e) {
                LOGGER.error(e);
                return false;
            }
        }else {
            return false;
            }
        return true;
    }

    @Override
    public Object delete(Object o, String id) {
        if (o instanceof Customer) {
            try (Session session = driver.session()) {
                session.writeTransaction((TransactionWork<Void>) tx -> {
                    tx.run("MATCH (c: Customer {id: $custId})-[r:Likes]-> (s: Song {id: $songId} ) "+
                            "DELETE r ", parameters("custId", ((Customer) o).getCustId().toString(),"songId" ,id));
                    return null;
                });
                session.writeTransaction((TransactionWork<Void>) tx -> {
                    tx.run("MATCH (a: Album) " +
                            "WHERE NOT (a)--()"+
                            "DETACH DELETE a ");
                    return null;
                });
                session.writeTransaction((TransactionWork<Void>) tx -> {
                    tx.run("MATCH (s: Song) " +
                            "WHERE NOT (s)--()"+
                            "DETACH DELETE s ");
                    return null;
                });
                session.writeTransaction((TransactionWork<Void>) tx -> {
                    tx.run("MATCH (c: Customer) " +
                            "WHERE NOT (c)--()"+
                            "DETACH DELETE c ");
                    return null;
                });

            } catch (Exception e) {
                LOGGER.error(e);
                return false;
            }
        } else {
            return false;
        }
        return o;
    }

    private Album getAlbum(Song s){
        Artist a = new Artist();
        a.setId(new ObjectId(s.getArtistkey()));
        a = (Artist) CRUDGenericManager.findMongo(a);
        return findAlbum(a,s);
    }

    private Album findAlbum(Artist a,Song s){
        for (Album album : a.getAlbums()){
            for (Song song : album.getSongs()){
                if (song.getName().equals(s.getName())){
                    return album;
                }
            }
        }
        return null;
    }
    private String getJson(Object o) {
        Gson gson = new GsonBuilder().create();
        return gson.toJson(o);
    }
}
