package it.dao;

import it.common.chain.IScope;
import it.common.model.Artist;
import it.common.model.Customer;
import it.common.model.Song;
import it.dao.mongo.CRUDMongoManager;
import it.dao.neo4j.CRUDNeo4jManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Map;

public final class CRUDGenericManager {

    private static final Logger LOGGER = LogManager.getLogger(CRUDGenericManager.class);
    private static CRUDMongoManager MONGOMAN = new CRUDMongoManager();
    private static CRUDNeo4jManager NEO4JMAN = new CRUDNeo4jManager();

    private static final String CUSTOMER = "customer";
    private static final String ARTIST = "artist";
    private static final String ARTISTS = "artists";

    public static final String MOSTFAVORITESONGS = "MOSTFAVORITESONGS";
    public static final String MOSTFAVORITESONGSFORGENRE = "MOSTFAVORITESONGSFORGENRE";
    public static final String MOSTFAVORITESONGSFORARTIST = "MOSTFAVORITESONGSFORARTIST";
    public static final String MEANWORDSFORSONGPERYEARS = "MEANWORDSFORSONGPERYEARS";
    public static final String MEANSONGSFORARTISTPERYEARS = "MEANSONGSFORARTISTPERYEARS";
    public static final String MOSTFAVORITEGENRES = "MOSTFAVORITEGENRES";
    public static final String MOSTUSEDWORDSFORARTIST = "MOSTUSEDWORDSFORARTIST";
    public static final String MOSTUSEDWORDSFORGENRE = "MOSTUSEDWORDSFORGENRE";
    public static final String MOSTFAVORITEARTISTS = "MOSTFAVORITEARTISTS";


    public static void setUpManager(IScope scope){
        MONGOMAN.setUpManager(scope);
        NEO4JMAN.setUpManager(scope);
    }

    public static void exitManager(){
        MONGOMAN.exitManager();
        NEO4JMAN.close();
    }

    public static void closeNeo(IScope scope){
        NEO4JMAN.close();
    }

    public static Object createNeo4j(Object o) {
        return NEO4JMAN.write(o);
    }

    public static Object updateNeo4j(Object o) {
        return NEO4JMAN.update(o);
    }

    public static Map<String, List<String>> readNeo4j(Object o) {return NEO4JMAN.read(o); }

    public static boolean deleteNeo4j(Object o, String id) {
        NEO4JMAN.delete(o, id);
        return true;
    }

    public static Object writePref(Object o){
        MONGOMAN.update(o);
        NEO4JMAN.write(o);
        return true;
    }

    public static Object writeMongo(Object o) {
        return MONGOMAN.write(o);
    }

    public static Object updateMongo(Object o) {
        return MONGOMAN.update(o);
    }

    public static Object getStatistic(Object o, String type){
        switch (type){
            case MOSTFAVORITESONGSFORARTIST:
                return MONGOMAN.getMostFavoriteSongForArtist((Artist) o);
            case MOSTFAVORITESONGSFORGENRE:
                return MONGOMAN.getMostFavoriteSongForGenre((String) o);
            case MOSTUSEDWORDSFORARTIST:
                return MONGOMAN.getMostUsedWordsForArtist((Artist) o);
            case MOSTUSEDWORDSFORGENRE:
                return MONGOMAN.getMostUsedWordsForGenre((String)o);
            default:
                LOGGER.error("case not contemplated");

        }
        return null;
    }

    public static Object getStatistic(String type){
        switch (type) {
            case MOSTFAVORITEGENRES:
                return MONGOMAN.getMostPreferGenre();
            case MOSTFAVORITESONGS:
                return MONGOMAN.getMostFavoriteSong();
            case MEANSONGSFORARTISTPERYEARS:
                return MONGOMAN.getMeanSongsForArtistPerYear();
            case MEANWORDSFORSONGPERYEARS:
                return MONGOMAN.getMeanWordsForSongPerYear();
            case MOSTFAVORITEARTISTS:
                return MONGOMAN.getArtistMostPref();
            default:
                LOGGER.error("case not contemplated");

        }
        return null;
    }

    public static List<Song> getRandomSongs(int i, Object o) {
        return MONGOMAN.getRandomsSong(i, (Customer) o);
    }

    /**
     * @param o comand the read:
     *          - Object Customer --> return a Customer Object
     *          - Object Artist --> return a Map<String,Artist>
     */
    public static Object findMongo(Object o) {
        if(o instanceof String){
            Artist a = new Artist();
            a.setName((String)o);
            return MONGOMAN.read(a);
        }
        return MONGOMAN.read(o);
    }

    public static boolean deleteMongo(Object o) {
        return (boolean) MONGOMAN.delete(o);
    }

}


