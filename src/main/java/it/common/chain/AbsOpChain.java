package it.common.chain;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public abstract class AbsOpChain {

    private final static Logger LOGGER = LogManager.getLogger(AbsOpChain.class);

    protected List<IOperation> chain = new ArrayList<IOperation>();

    protected abstract void populateChain();

    public IScope execChain(Map<String, String> params) throws Exception {

        IScope scope = new Scope();
        scope.addParameter("INIT_PARAMS", params);

        populateChain();

        // Esecuzione operazioni
        LOGGER.info("Avvio Chain");
        for (IOperation operation : chain) {
            operation.handleExec(scope);
        }
        LOGGER.info("Completamento Chain");

        return scope;
    }

}
