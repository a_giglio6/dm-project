package it.common.chain;

public interface IScope {

    String CUSTOMER = "customer";
    String USER = "user";
    String PASSWORD = "password";
    String DBNAME = "DBname";
    String DBCONN = "DBCONN" ;
    String MONGOCONN = "MONGOCONN";
    String NEOCONN = "NEOCONN";
    String NEOUSER = "NEOUSER";
    String NEOPASS = "NEOPASS";
    String SONG = "SONG";
    String ARTIST = "ARTIST";
    String CUSTOMERS = "CUSTOMERS";
    String SONGS = "songs";
    String ALBUMS = "albums";
    String STATISTIC = "statistic";
    String NAMESTATIC = "nameStat";
    String LASTPAGE = "lastpage";
    String MOOD = "mood";

    void addParameter(String key, Object value);
    Object getParameter(String key);
    void modifyParameter(String key, Object value);

}
