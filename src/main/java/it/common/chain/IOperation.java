package it.common.chain;

public interface IOperation {

    void handleExec(IScope scope) throws Exception;

}
