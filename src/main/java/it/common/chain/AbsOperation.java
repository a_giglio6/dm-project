package it.common.chain;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;

public abstract class AbsOperation implements IOperation {

    private static final Logger LOGGER = LogManager.getLogger(AbsOperation.class);

    protected static final String DBCONN = "DBCONN";

    @SuppressWarnings("unchecked")
    protected Map<String, String> getInitParams(IScope scope) {
        return (Map<String, String>) scope.getParameter("INIT_PARAMS");
    }

    @SuppressWarnings("unchecked")
    protected String getInitParam(IScope scope, String paramName) {
        Map<String, String> initParams = (Map<String, String>) scope.getParameter("INIT_PARAMS");
        return initParams.get(paramName);
    }

}
