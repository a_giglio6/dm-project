package it.common.interfa;

public interface CRUDManager {
    Object read(Object o);
    boolean write(Object o);
    Object update(Object o);
    Object delete(Object o, String id);
}
