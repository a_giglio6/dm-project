package it.common.interfa;


import org.bson.types.ObjectId;

public interface Client {

    ObjectId getCustId();
    void setCustId(ObjectId custId);
    String getFirstName();
    void setFirstName(String firstName);
    String getLastName();
    void setLastName(String lastName);
    String getCity();
    void setCity(String city);
    String getState();
    void setState(String state);
    String getAddress();
    void setAddress(String address);
    String getEmail();
    void setEmail(String email);

}
