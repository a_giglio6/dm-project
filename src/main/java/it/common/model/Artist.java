package it.common.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.bson.types.ObjectId;
import java.util.ArrayList;
import java.util.List;

public class Artist {

    @SerializedName("_id")
    @Expose
    private ObjectId id;

    @Expose
    private String name;

    @Expose
    private List<Album> albums;

    private List<Song> songs;

    public Artist() {
        songs = new ArrayList<>();
        albums = new ArrayList<>();
    }

    public ObjectId getId() { return id; }

    public void setId(ObjectId id) { this.id = id; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public List<Song> getSongs() { return songs; }

    public void addAlbum(Album a){ albums.add(a); }

    public void setSongs(List<Song> songs) { this.songs = songs; }

    public void addSong(Song song){
        songs.add(song);
    }

    public List<Album> getAlbums() { return albums; }

    public void setAlbums(List<Album> albums) { this.albums = albums; }

    public void removeSong(Song song){
        for (Album a: getAlbums()) {
            a.removeSong(song);
        }
    }

    public void updateSong(Song song){
        for (Album a: getAlbums()){
            a.updateSong(song);
        }
    }
}
