package it.common.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.bson.types.ObjectId;
import java.util.Date;

import java.util.List;

public class Song {

    @SerializedName("_id")
    @Expose
    private ObjectId id;

    @Expose
    private String name;

    @Expose
    private String lyrics;

    private Artist artist;

    @Expose
    private String artistkey;

    @Expose
    private String date;

    @Expose
    private String genre;

    private Album album;

    @Expose
    private List<String> feat;

    public Song() {
    }

    public ObjectId getId() { return id; }

    public void setId(ObjectId id) { this.id = id; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public String getLyrics() { return lyrics; }

    public void setLyrics(String lyrics) { this.lyrics = lyrics; }

    public Artist getArtist() { return artist; }

    public void setArtist(Artist artist) { this.artist = artist; }

    public String getGenre() { return genre;}

    public String getDate() { return date;}

    public void setDate(String date) { this.date = date;}

    public String getArtistkey() { return artistkey; }

    public void setArtistkey(String artistkey) { this.artistkey = artistkey; }

    public void setGenre(String genre) { this.genre = genre; }

    public Album getAlbum() { return album; }

    public void setAlbum(Album album) { this.album = album; }

    public List<String> getFeat() { return feat; }

    public void setFeat(List<String> feat) {this.feat = feat; }
}
