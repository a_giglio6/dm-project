package it.common.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import it.common.interfa.Client;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.List;

public class Customer implements Client {
    @SerializedName("_id")
    @Expose
    private ObjectId custId;

    @Expose
    private String firstName;

    @Expose
    private String lastName;

    @Expose
    private String email;

    @Expose
    private Login login;

    @Expose
    private String city;

    @Expose
    private String state;

    @Expose
    private String address;

    @Expose
    private String prefGen;

    @Expose
    private List<Song> prefSong;

    public Customer(ObjectId custId, String firstName, String lastName, String city, String state, String address, String email) {
        this.custId = custId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.city = city;
        this.state = state;
        this.address = address;
        this.email = email;
        prefSong = new ArrayList<>();
    }

    public Customer(String firstName, String lastName, String city, String state, String address, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.city = city;
        this.state = state;
        this.address = address;
        this.email = email;
        prefSong = new ArrayList<>();
    }

    public Customer() {
        prefSong = new ArrayList<>();
    }


    public ObjectId getCustId() {
        return custId;
    }

    public void setCustId(ObjectId custId) {
        this.custId = custId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Login getLogin() {
        return login;
    }

    public void addPrefSong(Song song){
        prefSong.add(song);
    }

    public void removePrefSong(Song song){
        prefSong.remove(song);
    }

    public void setLogin(Login login) { this.login = login; }

    public String getPrefGen() { return prefGen; }

    public void setPrefGen(String prefGen) { this.prefGen = prefGen; }

    public List<Song> getPrefSong() { return prefSong; }

    public void setPrefSong(List<Song> prefSong) { this.prefSong = prefSong; };

}
