package it.common.model;

import com.google.gson.annotations.Expose;
import it.dao.CRUDGenericManager;
import org.bson.types.ObjectId;

import java.util.*;

public class Album {
    @Expose
    private String name;
    @Expose
    private String date;
    @Expose
    private String genre;
    @Expose
    private List<Song> songs;

    private Artist artist;

    public Album() {
        songs = new ArrayList<>();
    }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public String getDate() { return date; }

    public void setDate(String date) { this.date = date; }

    public String getGenre() { return genre; }

    public void setGenre(String genre) { this.genre = genre; }

    public List<Song> getSongs() { return songs; }

    public void setSongs(List<Song> songs) { this.songs = songs; }

    public void setArtistOnSongs(ObjectId id){
        for (Song song : songs) {
            song.setArtistkey(id.toString());
        }
    }
    public Artist getArtist() { return artist; }

    public void addSong(Song song){
        songs.add(song);
    }

    public void setArtist(Artist artist) { this.artist = artist; }

    public void removeSong(Song song){
        Iterator iterator = songs.iterator();
        Song s;
        while(iterator.hasNext()) {
            s = (Song) iterator.next();
            if (s == null){
                iterator.remove();
            }
            else{
                if (s.getName().equals(song.getName())){
                    iterator.remove();
                    //songs.remove(s);
                }
            }
        }
    }

    public void addIdOnSongs(){
        List<Song> son = new ArrayList<>();
        Map<String,Song> tempMap;

        for (Song s: songs) {
            tempMap = (Map) CRUDGenericManager.findMongo(s);
            son.add(tempMap.get(s.getName()));
        }
        songs = son;
    }

    public void updateSong(Song song){
        List<Song> songs = new ArrayList<>();
        for (Song s: getSongs()) {
            if (s.getName().equals(song.getName())){
                songs.add(song);
            }
            else {
                songs.add(s);
            }
        }
        this.songs=songs;
    }
}
