package it.common.model;

import com.google.gson.annotations.Expose;

public class Login {
    @Expose
    private String user;
    @Expose
    private String password;
    @Expose
    private String authorization ;

    private Customer customer;

    public Login() {
        this.authorization = "USER";
    }

    public Login(String user, String password) {
        this.user = user;
        this.password = password;
        this.authorization = "USER";
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Customer getCustomer() {return customer; }

    public void setCustomer(Customer customer) { this.customer = customer; }

    public String getAuthorization() {
        return authorization;
    }

    public void setAuthorization(String authorization) {
        this.authorization = authorization;
    }
}
