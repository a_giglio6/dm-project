package it.common.gui.user;

import com.google.gson.Gson;
import it.common.chain.IScope;
import it.common.gui.LoginPage;
import it.common.gui.generic.FindPage;
import it.common.gui.generic.Mood;
import it.common.gui.generic.ShowUser;
import it.common.gui.generic.Statistics;
import it.common.model.Album;
import it.common.model.Artist;
import it.common.model.Customer;
import it.common.model.Song;
import it.dao.CRUDGenericManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.types.ObjectId;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class HomePageClient extends JFrame {

    private JPanel pannel;
    private JButton back;
    private JButton statistics;
    private JButton modifyUser;
    private JButton find;
    private JButton mood;
    private static final Logger LOGGER = LogManager.getLogger(HomePageClient.class);

    public HomePageClient(IScope scope) {
        super("FAVOURITES SONGS");
        pannel = new JPanel();
        JLabel name;
        JLabel name_s;
        JLabel name_a;
        JLabel album_s;
        JLabel fav;
        JLabel reccomendation;

        Customer customer = (Customer) scope.getParameter(IScope.USER);
        int size = customer.getPrefSong().size();
        int y = 40;
        try {
            Map<String, List<String>> songs = CRUDGenericManager.readNeo4j(customer);
            if (size == 0 || songs == null) {
                int count3 = 0;
                List<Song> randomSelection = CRUDGenericManager.getRandomSongs(5, customer);
                int sizeran = randomSelection.size();
                fav = new JLabel("RECCOMENDATION");
                pannel.add(fav);
                fav.setBounds(35, y, 150, 20);
                fav.setFont(new Font("Calibri", Font.BOLD, 16));
                y = y + 35;

                JButton add[] = new JButton[sizeran];

                for (Song sng : randomSelection) {
                    String prefson = sng.getName();
                    String prefalbum = sng.getAlbum().getName();
                    name_s = new JLabel(prefson);
                    pannel.add(name_s);
                    name_s.setBounds(35, y, 150, 20);
                    name_a = new JLabel(prefalbum);
                    pannel.add(name_a);
                    name_a.setBounds(200, y, 180, 20);
                    add[count3] = new JButton("ADD");
                    pannel.add(add[count3]);
                    add[count3].setBounds(370, y, 80, 20);

                    y = y + 35;
                    add[count3].addActionListener(e -> {
                        customer.addPrefSong(sng);
                        CRUDGenericManager.writePref(customer);
                        scope.addParameter(IScope.USER, customer);

                        JOptionPane.showMessageDialog(null, "Song is added on pref");
                        dispose();
                        HomePageClient homePageClient = new HomePageClient(scope);
                        dispose();
                    });
                    count3++;
                }
            } else {

                int lenght = songs.size();

                JButton add[] = new JButton[lenght];
                JButton delete[] = new JButton[size];
                int count = 0;
                int count2 = 0;

                fav = new JLabel("FAVOURITES");
                pannel.add(fav);
                fav.setBounds(35, y, 150, 20);
                fav.setFont(new Font("Calibri", Font.BOLD, 16));
                y = y + 35;

                for (int j = 0; j < size; j++) {

                    Song sng = customer.getPrefSong().get(j);

                    Album a = null;
                    if (sng.getAlbum()==null){
                        a = getAlbum(sng);
                    }else {
                        a = sng.getAlbum();
                    }

                    a.setArtist(null);
                    a.setSongs(new ArrayList<>());

                    sng.setAlbum(a);
                    sng.setArtist(null);

                    String prefson = customer.getPrefSong().get(j).getName();
                    String prefalbum = a.getName();

                    name_s = new JLabel(prefson);
                    pannel.add(name_s);
                    name_s.setBounds(35, y, 150, 20);
                    name_a = new JLabel(prefalbum);
                    pannel.add(name_a);
                    name_a.setBounds(200, y, 180, 20);

                    y = y + 35;
                    count2++;
                }

                reccomendation = new JLabel("RECCOMENDATION");
                reccomendation.setFont(new Font("Calibri", Font.BOLD, 16));
                pannel.add(reccomendation);
                reccomendation.setBounds(35, y, 150, 20);
                y = y + 35;

                    for (Map.Entry<String, List<String>> entry : songs.entrySet()) {

                        String son = entry.getValue().get(0);
                        String album_json = entry.getValue().get(1);
                        String object = entry.getKey();

                        Gson g = new Gson();
                        Song p = g.fromJson(object, Song.class);
                        boolean check = isVariablesDifferent(customer.getPrefSong(), son);

                        if (check) {
                            Gson g2 = new Gson();
                            Album p2 = g2.fromJson(album_json, Album.class);
                            String album = p2.getName();
                            p.setAlbum(p2);

                            name = new JLabel(son);
                            album_s = new JLabel(album);

                            add[count] = new JButton("ADD");
                            pannel.add(add[count]);
                            pannel.add(name);
                            pannel.add(album_s);

                            add[count].setBounds(385, y, 80, 20);
                            name.setBounds(35, y, 150, 20);
                            album_s.setBounds(200, y, 180, 20);
                            y = y + 35;

                            add[count].addActionListener(e -> {
                                customer.addPrefSong(p);
                                CRUDGenericManager.writePref(customer);
                                scope.addParameter(IScope.USER, customer);
                                JOptionPane.showMessageDialog(null, "Song is added on pref");
                                dispose();
                                HomePageClient homePageClient = new HomePageClient(scope);
                                dispose();
                            });
                            count++;
                        }
                    }

                }

        } catch (Exception e) {
            LOGGER.error(e);
        }


        back = new JButton("BACK");
        back.addActionListener(e -> {
            LoginPage loginPage = new LoginPage(scope);
            dispose();
        });

        statistics = new JButton("STATISTICS");
        statistics.addActionListener(e -> {
            Statistics statistics = new Statistics(scope);
            dispose();
        });

        mood = new JButton("HOW ARE YOU FEELING TODAY?");
        mood.addActionListener(e -> {
            Mood mood = new Mood(scope);
            dispose();
        });


        modifyUser = new JButton("MODIFY YOUR PREF");
        modifyUser.addActionListener(e->{
            scope.addParameter(IScope.CUSTOMER,scope.getParameter(IScope.USER));
            ShowUser showUser = new ShowUser(scope);
            dispose();
        });

        find = new JButton("FIND");
        find.addActionListener(e -> {
            FindPage findPage = new FindPage(scope);
            dispose();
        });

        setSize(500, y + 250);
        setLocation(500, 280);
        pannel.setLayout(null);

        back.setBounds(10, y + 35, 150, 20);
        pannel.add(back);
        statistics.setBounds(170, y + 35, 150, 20);
        modifyUser.setBounds(170,y+70,150,20);
        mood.setBounds(120, y + 105, 250, 20);
        pannel.add(modifyUser);
        pannel.add(statistics);
        pannel.add(mood);
        find.setBounds(330, y + 35, 150, 20);
        pannel.add(find);

        getContentPane().add(pannel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }
    private Album getAlbum(Song s){
        Artist a = new Artist();
        a.setId(new ObjectId(s.getArtistkey()));
        a = (Artist) CRUDGenericManager.findMongo(a);
        return findAlbum(a,s);
    }

    private Album findAlbum(Artist a,Song s){
        List<Album> albums= a.getAlbums();
        for (Album album : a.getAlbums()){
            for (Song song : album.getSongs()){
                if (song.getName().equals(s.getName())){
                    return album;
                }
            }
        }
        return null;
    }

    private boolean isVariablesDifferent(List<Song> songs, String son){
        for(int i = 0; i < songs.size(); i++){
            String s = songs.get(i).getName();
            if(s.equalsIgnoreCase(son)){
                return false;
            }
        }
        return true;
    }


}
