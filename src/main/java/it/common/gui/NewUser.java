package it.common.gui;

import it.common.chain.IScope;
import it.common.interfa.Client;
import it.common.model.Customer;
import it.common.model.Login;
import it.dao.CRUDGenericManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.util.Map;

public class NewUser extends JFrame {
    private JButton create;
    private JPanel newUserPanel;
    private JTextField txuserer;
    private JTextField passer;
    private JTextField txfirstname;
    private JTextField txlastname;
    private JTextField txcity;
    private JTextField txstate;
    private JTextField txaddress;
    private JTextField txemail;
    private JButton back;

    private JTextField txprefGen;

    private JLabel username;
    private JLabel password;
    private JLabel firstName;
    private JLabel lastName;
    private JLabel city;
    private JLabel state;
    private JLabel address;
    private JLabel email;
    private JLabel prefGen;


    private IScope scope;

    private static final Logger LOGGER = LogManager.getLogger(NewUser.class);


    public NewUser(IScope scope) {
        super("Registration");

        back = new JButton("BACK");
        create = new JButton("SIGN UP");
        username = new JLabel("User - ");
        password = new JLabel("Pass - ");
        firstName = new JLabel("Name - ");
        lastName = new JLabel("Surname - ");
        city = new JLabel("City - ");
        state = new JLabel("State - ");
        address = new JLabel("Address - ");
        email = new JLabel("Email - ");
        prefGen = new JLabel("What is your pref gen? - ");

        newUserPanel = new JPanel();
        txuserer = new JTextField(15);
        txfirstname = new JTextField(15);
        passer = new JPasswordField(15);
        txlastname = new JTextField(15);
        txcity = new JTextField(15);
        txstate = new JTextField(15);
        txaddress = new JTextField(15);
        txemail = new JTextField(15);

        txprefGen = new JTextField(15);


        setSize(300, 420);
        setLocation(500, 280);
        newUserPanel.setLayout(null);


        txuserer.setBounds(90, 30, 150, 20);
        passer.setBounds(90, 65, 150, 20);
        create.setBounds(70, 340, 150, 20);
        username.setBounds(20, 28, 80, 20);
        password.setBounds(20, 63, 80, 20);
        firstName.setBounds(20, 93, 80, 20);
        lastName.setBounds(20, 130, 80, 20);

        txfirstname.setBounds(90, 95, 150, 20);
        txlastname.setBounds(90, 132, 150, 20);
        city.setBounds(20, 160, 80, 20);
        state.setBounds(20, 190, 80, 20);
        txcity.setBounds(90, 162, 150, 20);
        txstate.setBounds(90, 192, 150, 20);
        address.setBounds(20, 220, 80, 20);
        email.setBounds(20, 250, 80, 20);
        txaddress.setBounds(90, 222, 150, 20);
        txemail.setBounds(90, 252, 150, 20);

        prefGen.setBounds(20, 280, 200, 20);

        txprefGen.setBounds(160, 280, 100, 20);
        back.setBounds(70, 310, 150, 20);

        newUserPanel.add(create);
        newUserPanel.add(txuserer);
        newUserPanel.add(passer);
        newUserPanel.add(txfirstname);
        newUserPanel.add(txlastname);
        newUserPanel.add(txaddress);
        newUserPanel.add(txcity);
        newUserPanel.add(txemail);
        newUserPanel.add(txprefGen);
        newUserPanel.add(txstate);
        newUserPanel.add(username);
        newUserPanel.add(password);

        newUserPanel.add(firstName);
        newUserPanel.add(lastName);
        newUserPanel.add(address);
        newUserPanel.add(city);
        newUserPanel.add(email);
        newUserPanel.add(prefGen);
        newUserPanel.add(state);
        newUserPanel.add(back);

        getContentPane().add(newUserPanel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

        back.addActionListener(e -> {
            LoginPage user = new LoginPage(scope);
            dispose();

        });

        create.addActionListener(e -> {
            try {

                String punamer = txuserer.getText();
                String ppaswder = passer.getText();

                String firstName = txfirstname.getText();
                String lastName = txlastname.getText();
                String city = txcity.getText();
                String state = txstate.getText();
                String address = txaddress.getText();
                String email = txemail.getText();
                String genre = txprefGen.getText();


                Customer customer = new Customer();

                if (clientExist(punamer)) {
                    JOptionPane.showMessageDialog(null, "Username is already in use");
                    txuserer.setText("");
                    passer.setText("");
                    txuserer.requestFocus();

                } else if (punamer.equals("") && ppaswder.equals("")) {
                    JOptionPane.showMessageDialog(null, "Please insert Username and Password");
                } else {
                    if (punamer.equals("") || ppaswder.equals("") || genre.equals("") || address.equals("")||lastName.equals("") || firstName.equals("")||state.equals("") && email.equals("") ||
                            city.equals("") ){
                        JOptionPane.showMessageDialog(null, "Please insert all");
                    }


                    Login login = new Login();
                    login.setUser(punamer);
                    login.setPassword(ppaswder);
                    login.setCustomer(customer);
                    customer.setPrefGen(genre);
                    customer.setAddress(address);
                    customer.setLastName(lastName);
                    customer.setFirstName(firstName);
                    customer.setState(state);
                    customer.setEmail(email);
                    customer.setCity(city);
                    customer.setLogin(login);
                    CRUDGenericManager.writeMongo(customer);
                    JOptionPane.showMessageDialog(null, "Account has been created.");
                    dispose();
                    LoginPage log = new LoginPage(scope);

                }
            } catch (Exception d) {
                d.printStackTrace();
            }

        });
    }

    private boolean clientExist(String user) {
        Customer c = new Customer();
        c.setLogin(new Login());
        c.getLogin().setUser(user);
        Object result = CRUDGenericManager.findMongo(c);
        if (result==null){
            return false;
        }
        else if (result instanceof Map){
            if (((Map) result).isEmpty()){
                return false;
            }
        }
        return true;
    }

}
