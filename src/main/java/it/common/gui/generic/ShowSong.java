package it.common.gui.generic;

import it.common.chain.IScope;
import it.common.model.*;
import it.dao.CRUDGenericManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;

public class ShowSong extends JFrame {
    private JPanel pannel;
    private JButton modify;
    private JButton select;
    private JButton delete;
    private JButton back;

    private JTextField name;
    private JTextField lyrics;
    private JTextField date;
    private JLabel nameLabel;
    private JLabel lyricsLabel;
    private JLabel dateLabel;
    private JLabel nameFix;
    private JLabel lyricsFix;
    private JLabel dateFix;


    private static final Logger LOGGER = LogManager.getLogger(ShowSong.class);


    public ShowSong(IScope scope) {
        super("SONG");

        Song s = (Song) scope.getParameter(IScope.SONG);
        boolean superUser = ((Customer) scope.getParameter(IScope.USER)).getLogin().getAuthorization().equals("SUPERUSER");

        back = new JButton("BACK");
        select = new JButton("ADD TO PREF");
        delete = new JButton("DELETE");
        modify = new JButton("MODIFY");

        nameLabel = new JLabel("Name - ");
        lyricsLabel = new JLabel("Lyrics - ");
        dateLabel = new JLabel("date - ");
        nameFix = new JLabel(s.getName());
        lyricsFix = new JLabel(s.getLyrics());
        dateFix = new JLabel(s.getDate());

        pannel = new JPanel();
        name = new JTextField(s.getName(), 15);
        lyrics = new JTextField(s.getLyrics(), 15);
        date = new JTextField(s.getDate(), 15);

        setSize(300, 300);
        setLocation(500, 280);
        pannel.setLayout(null);


        back.setBounds(75, 140, 150, 20);
        nameLabel.setBounds(35, 30, 150, 20);
        lyricsLabel.setBounds(35, 60, 80, 20);
        dateLabel.setBounds(35, 90, 80, 20);
        delete.setBounds(75, 170, 150, 20);
        modify.setBounds(75, 200, 150, 20);
        name.setBounds(105, 30, 150, 20);
        lyrics.setBounds(105, 60, 150, 20);
        date.setBounds(105, 90, 150, 20);
        select.setBounds(75, 170, 150, 20);
        nameFix.setBounds(105, 30, 150, 20);
        lyricsFix.setBounds(105, 60, 150, 20);
        dateFix.setBounds(105, 90, 150, 20);


        if (superUser) {
            pannel.add(delete);
            pannel.add(modify);
            pannel.add(name);
            pannel.add(lyrics);
            pannel.add(date);
        } else {
            pannel.add(select);
            pannel.add(nameFix);
            pannel.add(lyricsFix);
            pannel.add(dateFix);
        }
        pannel.add(nameLabel);
        pannel.add(lyricsLabel);
        pannel.add(dateLabel);

        pannel.add(back);

        getContentPane().add(pannel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

        back.addActionListener(e -> {
            if ((scope.getParameter(IScope.LASTPAGE)).equals(FindPage.class.getName())){
                scope.modifyParameter(IScope.LASTPAGE,"");
                FindPage findPage = new FindPage(scope);
                dispose();
            }else {
                ListSongs listSongs = new ListSongs(scope);
                dispose();
            }
        });

        delete.addActionListener(e -> {
            try {
                if (CRUDGenericManager.deleteMongo(s)) {
                    JOptionPane.showMessageDialog(null, "SONG DELETE");
                } else {
                    JOptionPane.showMessageDialog(null, "Error when try to delete");
                }
                if ((scope.getParameter(IScope.LASTPAGE)).equals(FindPage.class.getName())){
                    scope.modifyParameter(IScope.LASTPAGE,"");
                    FindPage findPage = new FindPage(scope);
                    dispose();
                }else {
                    ListSongs listSongs = new ListSongs(scope);
                    dispose();
                }

            } catch (Exception ex) {
                LOGGER.error(ex);
            }
        });

        select.addActionListener(e -> {
            try {
                Customer customer = ((Customer) scope.getParameter(IScope.USER));
                customer.addPrefSong(s);
                CRUDGenericManager.writePref(customer);
                if ((scope.getParameter(IScope.LASTPAGE)).equals(FindPage.class.getName())){
                    scope.modifyParameter(IScope.LASTPAGE,"");
                    FindPage findPage = new FindPage(scope);
                    dispose();
                }else {
                    ListSongs listSongs = new ListSongs(scope);
                    dispose();
                }

            } catch (Exception ex) {
                LOGGER.error(ex);
            }
        });

        modify.addActionListener(e -> {
            try {

                String nameSong = name.getText();
                String lyricsSong = lyrics.getText();
                String dateSong = date.getText();

                s.setName(nameSong);
                s.setLyrics(lyricsSong);
                s.setDate(dateSong);

                if ((boolean) CRUDGenericManager.updateMongo(s)) {
                    if ((boolean)CRUDGenericManager.updateNeo4j(s)){
                        scope.modifyParameter(IScope.SONG, s);
                        LOGGER.info("UPDATE OK");
                        JOptionPane.showMessageDialog(null, "UPDATE OK");
                    }
                }

                ShowSong showSong = new ShowSong(scope);
                dispose();

            } catch (Exception d) {
                LOGGER.info("UPDATE ERROR");
                JOptionPane.showMessageDialog(null, "UPDATE ERROR");
                LOGGER.error(d);
            }

        });
    }
}
