package it.common.gui.generic;

import it.common.chain.IScope;
import it.common.model.Customer;
import it.common.model.Song;
import it.dao.CRUDGenericManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.util.List;

public class ListSongs extends JFrame {
    private JPanel pannel;
    private JButton back;

    private String song;
    private static final Logger LOGGER = LogManager.getLogger(ListSongs.class);


    public ListSongs(IScope scope) {
        super("LIST SONGS");
        pannel = new JPanel();

        boolean superUser = ((Customer) scope.getParameter(IScope.USER)).getLogin().getAuthorization().equals("SUPERUSER");
        List<Song> s = ((List) scope.getParameter(IScope.SONGS));

        int lenght = s.size();
        JButton[] show = new JButton[lenght];
        JButton[] delete = new JButton[lenght];
        JButton[] select = new JButton[lenght];

        int count = 0;
        int y = 40;
        JLabel name;
        for (Song c : s) {

            song = c.getName();
            name = new JLabel(song);

            show[count] = new JButton("SHOW");
            delete[count] = new JButton("DELETE");
            select[count] = new JButton("SELECT");

            if (superUser) {
                pannel.add(delete[count]);
            } else {
                pannel.add(select[count]);
            }

            pannel.add(show[count]);
            pannel.add(name);

            delete[count].setBounds(400, y, 80, 20);
            show[count].setBounds(300, y, 80, 20);
            select[count].setBounds(190, y, 80, 20);
            name.setBounds(35, y, 150, 20);
            y = y + 35;

            show[count].addActionListener(e -> {
                scope.addParameter(IScope.SONG, c);
                ShowSong showSong = new ShowSong(scope);
                dispose();
            });

            select[count].addActionListener(e -> {
                try {
                    Customer customer = ((Customer) scope.getParameter(IScope.USER));
                    customer.addPrefSong(c);
                    scope.addParameter(IScope.USER, customer);
                    CRUDGenericManager.writePref(customer);

                    ListSongs listSongs = new ListSongs(scope);
                    dispose();
                } catch (Exception ex) {
                    LOGGER.error(ex);
                }
            });

            delete[count].addActionListener(e -> {
                try {
                    if (CRUDGenericManager.deleteMongo(c)) {
                        c.getAlbum().getSongs().remove(c);
                        c.getArtist().getSongs().remove(c);
                        JOptionPane.showMessageDialog(null, "SONG DELETE");
                    } else {
                        JOptionPane.showMessageDialog(null, "Error when try to delete");
                    }
                    ListSongs listSongs = new ListSongs(scope);
                    dispose();

                } catch (Exception ex) {
                    LOGGER.error(ex);
                }
            });
            count++;
        }
        back = new JButton("BACK");

        back.setBounds(135, y, 150, 20);

        setSize(450, y + 90);
        setLocation(500, 280);
        pannel.setLayout(null);

        pannel.add(back);

        getContentPane().add(pannel);
        getContentPane().add(pannel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

        back.addActionListener(e -> {
            ListAlbums listAlbums = new ListAlbums(scope);
            dispose();

        });

    }
}
