package it.common.gui.generic;

import it.common.chain.IScope;
import it.common.gui.LoginPage;
import it.common.gui.admin.MenuAdmin;
import it.common.gui.user.HomePageClient;
import it.common.model.Album;
import it.common.model.Artist;
import it.common.model.Customer;
import it.common.model.Song;
import it.dao.CRUDGenericManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.util.Map;

public class FindPage extends JFrame {
    private JPanel pannel;
    private JTextField artist;
    private JTextField song;
    private JButton back;
    private JButton find;
    private JLabel artistLable;
    private JLabel songLable;

    private static final String USER = "USER";
    private static final String SUPERUSER = "SUPERUSER";

    private static final Logger LOGGER = LogManager.getLogger(LoginPage.class);

    public FindPage(IScope scope) {
        super("FIND WHAT?");
        scope.addParameter(IScope.LASTPAGE,getClass().getName());

        pannel = new JPanel();

        artist = new JTextField(15);
        song = new JTextField(15);

        artistLable = new JLabel("Artist - ");
        songLable = new JLabel("Song - ");

        find = new JButton("FIND");
        back = new JButton("BACK");

        setSize(300, 230);
        setLocation(500, 280);
        pannel.setLayout(null);


        artistLable.setBounds(20, 28, 80, 20);
        songLable.setBounds(20, 63, 80, 20);
        artist.setBounds(70, 30, 150, 20);
        song.setBounds(70, 65, 150, 20);

        back.setBounds(30, 100, 80, 20);
        find.setBounds(130, 100, 80, 20);

        pannel.add(artistLable);
        pannel.add(songLable);
        pannel.add(artist);
        pannel.add(song);
        pannel.add(back);
        pannel.add(find);

        getContentPane().add(pannel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

        find.addActionListener(e -> {
            try {
                String findString;
                if (artist.getText().equals("") && song.getText().equals("")) {
                    JOptionPane.showMessageDialog(null, "Please insert one element");
                } else if (!artist.getText().equals("") && !song.getText().equals("")) {
                    JOptionPane.showMessageDialog(null, "Please insert one element");
                } else if (!artist.getText().equals("")) {
                    findString = artist.getText();
                    Artist a = new Artist();
                    a.setName(findString);
                    a = (Artist) CRUDGenericManager.findMongo(a);
                    if (a != null) {
                        scope.addParameter(IScope.ARTIST, a);
                        ShowArtist showArtist = new ShowArtist(scope);
                        dispose();
                    } else {
                        JOptionPane.showMessageDialog(null, "item not found");
                    }
                } else if (!song.getText().equals("")) {
                    findString = song.getText();
                    Song s = new Song();
                    s.setName(findString);
                    s = (Song) ((Map) CRUDGenericManager.findMongo(s)).get(findString);
                    if (s != null) {
                        scope.addParameter(IScope.SONG, s);
                        ShowSong showSong = new ShowSong(scope);
                        dispose();
                    } else {
                        JOptionPane.showMessageDialog(null, "item not found");
                    }
                }
            } catch (Exception ex) {
                LOGGER.error(ex);
            }
        });


        back.addActionListener(e -> {
            if (((Customer) scope.getParameter(IScope.USER)).getLogin().getAuthorization().equals(SUPERUSER)) {
                MenuAdmin menuAdmin = new MenuAdmin(scope);
                dispose();
            } else {
                HomePageClient homePageClient = new HomePageClient(scope);
                dispose();
            }

        });


    }
}
