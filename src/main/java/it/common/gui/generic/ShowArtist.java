package it.common.gui.generic;

import it.common.chain.IScope;
import it.common.model.Artist;
import it.common.model.Customer;
import it.dao.CRUDGenericManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;

public class ShowArtist extends JFrame {
    private JPanel pannel;
    private JButton modify;
    private JButton showAlbums;
    private JButton delete;
    private JButton back;

    private JTextField name;
    private JLabel nameLabel;
    private JLabel nameFix;

    private static final Logger LOGGER = LogManager.getLogger(ShowArtist.class);

    public ShowArtist(IScope scope) {
        super("ARTIST");

        scope.modifyParameter(IScope.LASTPAGE,"");
        Artist a = (Artist) scope.getParameter(IScope.ARTIST);
        boolean superUser = ((Customer) scope.getParameter(IScope.USER)).getLogin().getAuthorization().equals("SUPERUSER");

        back = new JButton("BACK");
        showAlbums = new JButton("SHOW ALBUMS");
        delete = new JButton("DELETE");
        modify = new JButton("MODIFY");

        nameLabel = new JLabel("Name - ");
        nameFix = new JLabel(a.getName());
        pannel = new JPanel();
        name = new JTextField(a.getName(), 15);

        setSize(300, 210);
        setLocation(500, 280);
        pannel.setLayout(null);

        back.setBounds(75, 65, 150, 20);
        nameLabel.setBounds(35, 30, 150, 20);
        delete.setBounds(75, 95, 150, 20);
        modify.setBounds(75, 125, 150, 20);
        name.setBounds(105, 30, 150, 20);
        showAlbums.setBounds(75, 100, 150, 20);
        nameFix.setBounds(105, 30, 150, 20);


        if (superUser) {
            pannel.add(delete);
            pannel.add(modify);
            pannel.add(name);
        } else {
            pannel.add(showAlbums);
            pannel.add(nameFix);
        }
        pannel.add(nameLabel);

        pannel.add(back);

        getContentPane().add(pannel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

        back.addActionListener(e -> {
            FindPage findPage = new FindPage(scope);
            dispose();
        });

        delete.addActionListener(e -> {
            try {
                if (CRUDGenericManager.deleteMongo(a)) {
                    JOptionPane.showMessageDialog(null, "Artist DELETE");
                } else {
                    JOptionPane.showMessageDialog(null, "Error when try to delete");
                }
                FindPage findPage = new FindPage(scope);
                dispose();

            } catch (Exception ex) {
                LOGGER.error(ex);
            }
        });

        showAlbums.addActionListener(e -> {
            try {
                scope.addParameter(IScope.ALBUMS,a.getAlbums());
                ListAlbums listAlbums = new ListAlbums(scope);
                dispose();
            } catch (Exception ex) {
                LOGGER.error(ex);
            }
        });

        modify.addActionListener(e -> {
            try {

                String nameSong = name.getText();
                a.setName(nameSong);

                if ((boolean) CRUDGenericManager.updateMongo(a)) {
                    scope.modifyParameter(IScope.ARTIST, a);
                    LOGGER.info("UPDATE OK");
                    JOptionPane.showMessageDialog(null, "UPDATE OK");
                } else {
                    LOGGER.info("UPDATE ERROR");
                    JOptionPane.showMessageDialog(null, "UPDATE ERROR");
                }

                ShowArtist showArtist = new ShowArtist(scope);
                dispose();

            } catch (Exception d) {
                LOGGER.error(d);
            }

        });
    }
}
