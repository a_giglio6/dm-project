package it.common.gui.generic;

import it.common.chain.IScope;
import it.common.gui.admin.ListUsers;
import it.common.model.Album;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.util.List;

public class ListAlbums extends JFrame {
    private JPanel pannel;
    private JButton back;
    private String name;
    private static final Logger LOGGER = LogManager.getLogger(ListUsers.class);


    public ListAlbums(IScope scope) {
        super("LIST ALBUMS");
        pannel = new JPanel();

        List<Album> albums = ((List) scope.getParameter(IScope.ALBUMS));

        int lenght = albums.size();
        JButton[] show = new JButton[lenght];

        int count = 0;
        int y = 40;
        JLabel name;
        for (Album a : albums) {

            this.name = a.getName();
            name = new JLabel(this.name);

            show[count] = new JButton("SHOW");
            pannel.add(show[count]);
            pannel.add(name);

            show[count].setBounds(160, y, 80, 20);
            name.setBounds(35, y, 130, 20);
            y = y + 35;

            show[count].addActionListener(e -> {
                scope.addParameter(IScope.SONGS, a.getSongs());
                ListSongs listSongs = new ListSongs(scope);
                dispose();
            });
            count++;
        }
        back = new JButton("BACK");
        back.setBounds(75, y, 150, 20);

        setSize(300, y + 90);
        setLocation(500, 280);
        pannel.setLayout(null);

        pannel.add(back);

        getContentPane().add(pannel);
        getContentPane().add(pannel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

        back.addActionListener(e -> {
            ShowArtist showArtist = new ShowArtist(scope);
            dispose();
        });

    }

}