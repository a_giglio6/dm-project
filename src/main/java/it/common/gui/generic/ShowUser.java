package it.common.gui.generic;

import it.common.chain.IScope;
import it.common.gui.admin.FindUserPage;
import it.common.gui.admin.ListUsers;
import it.common.gui.user.HomePageClient;
import it.common.model.Customer;
import it.common.model.Login;
import it.dao.CRUDGenericManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.types.ObjectId;

import javax.swing.*;
import java.util.Map;

public class ShowUser extends JFrame {
    private JPanel pannel;
    private JButton modify;
    private JButton delete;
    private JButton back;
    private JTextField txusername;
    private JTextField txpassword;
    private JTextField txfirstname;
    private JTextField txlastname;
    private JTextField txcity;
    private JTextField txstate;
    private JTextField txaddress;
    private JTextField txemail;
    private JTextField txgenre;
    private JLabel username;
    private JLabel password;
    private JLabel firstName;
    private JLabel lastName;
    private JLabel city;
    private JLabel state;
    private JLabel address;
    private JLabel email;
    private JLabel genre;


    private static final Logger LOGGER = LogManager.getLogger(ShowUser.class);


    public ShowUser(IScope scope) {
        super("Registration");

        Customer user = (Customer) scope.getParameter(IScope.USER);
        Map<ObjectId, Customer> customers = ((Map<ObjectId, Customer>) scope.getParameter(IScope.CUSTOMERS));
        Customer customer = (Customer) scope.getParameter(IScope.CUSTOMER);
        boolean superUser = user.getLogin().getAuthorization().equals("SUPERUSER");

        back = new JButton("BACK");
        delete = new JButton("DELETE");
        modify = new JButton("MODIFY");
        username = new JLabel("User - ");
        password = new JLabel("Pass - ");
        firstName = new JLabel("Name - ");
        lastName = new JLabel("Surname - ");
        city = new JLabel("City - ");
        state = new JLabel("State - ");
        address = new JLabel("Address - ");
        email = new JLabel("Email - ");
        genre = new JLabel("Genre -");


        pannel = new JPanel();
        txusername = new JTextField(customer.getLogin().getUser(), 15);
        txpassword = new JTextField(customer.getLogin().getPassword(), 15);
        txfirstname = new JTextField(customer.getFirstName(), 15);
        txlastname = new JTextField(customer.getLastName(), 15);
        txcity = new JTextField(customer.getCity(), 15);
        txstate = new JTextField(customer.getState(), 15);
        txaddress = new JTextField(customer.getAddress(), 15);
        txemail = new JTextField(customer.getEmail(), 15);
        txgenre = new JTextField(customer.getPrefGen(),15);


        setSize(300, 450);
        setLocation(500, 280);
        pannel.setLayout(null);

        modify.setBounds(75, 320, 150, 20);
        back.setBounds(75, 380, 150, 20);
        delete.setBounds(75, 350, 150, 20);
        username.setBounds(35, 30, 80, 20);
        password.setBounds(35, 60, 80, 20);
        txusername.setBounds(105, 30, 150, 20);
        txpassword.setBounds(105, 60, 150, 20);
        firstName.setBounds(35, 90, 80, 20);
        lastName.setBounds(35, 120, 80, 20);
        txfirstname.setBounds(105, 90, 150, 20);
        txlastname.setBounds(105, 120, 150, 20);
        city.setBounds(35, 150, 80, 20);
        state.setBounds(35, 180, 80, 20);
        txcity.setBounds(105, 150, 150, 20);
        txstate.setBounds(105, 180, 150, 20);
        address.setBounds(35, 210, 80, 20);
        email.setBounds(35, 240, 80, 20);
        genre.setBounds(35,270,80,20);
        txaddress.setBounds(105, 210, 150, 20);
        txemail.setBounds(105, 240, 150, 20);
        txgenre.setBounds(105,270,150,20);

        pannel.add(modify);
        if (superUser) {
            pannel.add(delete);
        }

        pannel.add(txusername);
        pannel.add(txpassword);
        pannel.add(username);
        pannel.add(password);

        pannel.add(txfirstname);
        pannel.add(txlastname);
        pannel.add(txaddress);
        pannel.add(txcity);
        pannel.add(txemail);
        pannel.add(txstate);
        pannel.add(txgenre);
        pannel.add(firstName);
        pannel.add(lastName);
        pannel.add(address);
        pannel.add(city);
        pannel.add(email);
        pannel.add(genre);

        pannel.add(state);
        pannel.add(back);

        getContentPane().add(pannel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

        back.addActionListener(e -> {
            if (superUser) {
                ListUsers listUsers = new ListUsers(scope);
                dispose();
            } else {
                HomePageClient homePageClient = new HomePageClient(scope);
                dispose();
            }

        });

        delete.addActionListener(e -> {
            try {
                if (CRUDGenericManager.deleteMongo(customer)) {
                    customers.remove(customer.getCustId());
                    scope.modifyParameter(IScope.CUSTOMER, customers);
                    JOptionPane.showMessageDialog(null, "USER DELETE");
                } else {
                    JOptionPane.showMessageDialog(null, "Error when try to delete");
                }

                FindUserPage findUserPage = new FindUserPage(scope);
                dispose();

            } catch (Exception ex) {
                LOGGER.error(ex);
            }
        });

        modify.addActionListener(e -> {
            try {

                String userName = txusername.getText();
                String password = txpassword.getText();
                String firstName = txfirstname.getText();
                String lastName = txlastname.getText();
                String city = txcity.getText();
                String state = txstate.getText();
                String address = txaddress.getText();
                String email = txemail.getText();
                String genre = txgenre.getText();

                if (clientExist(userName) && !userName.equals(customer.getLogin().getUser())) {
                    JOptionPane.showMessageDialog(null, "Username is already in use");
                    txusername.setText("");
                    txpassword.setText("");
                    txusername.requestFocus();
                } else {
                    Customer cust = (Customer) scope.getParameter(IScope.CUSTOMER);

                    if (cust == null) {
                        LOGGER.error("Client empty or incorrect");
                        JOptionPane.showMessageDialog(null, "Client empty or incorrect");
                    } else {
                        cust.getLogin().setUser(userName);
                        cust.getLogin().setPassword(password);
                        cust.setFirstName(firstName);
                        cust.setLastName(lastName);
                        cust.setCity(city);
                        cust.setState(state);
                        cust.setAddress(address);
                        cust.setEmail(email);
                        cust.setPrefGen(genre);

                        if ((boolean) CRUDGenericManager.updateMongo(cust)) {
                            LOGGER.info("UPDATE OK");
                            JOptionPane.showMessageDialog(null, "UPDATE OK");
                            scope.modifyParameter(IScope.CUSTOMER, cust);

                            if (superUser) {
                                ListUsers listUsers = new ListUsers(scope);
                                dispose();
                            } else {
                                HomePageClient homePageClient = new HomePageClient(scope);
                                dispose();
                            }
                        } else {
                            LOGGER.info("UPDATE ERROR");
                            JOptionPane.showMessageDialog(null, "UPDATE ERROR");
                        }
                    }
                }

            } catch (Exception d) {
                LOGGER.error(d);
            }

        });
    }

    private boolean clientExist(String user) {
        Customer c = new Customer();
        c.setLogin(new Login());
        c.getLogin().setUser(user);
        return CRUDGenericManager.findMongo(c) != null;
    }
}

