package it.common.gui.generic;

import it.common.chain.IScope;
import it.common.model.Song;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.util.Map;

public class ShowStatistic extends JFrame {
    private JPanel pannel;
    private JButton back;

    private String data;
    private Long numLong;
    private Double numDouble;
    private static final Logger LOGGER = LogManager.getLogger(ShowStatistic.class);


    public ShowStatistic(IScope scope) {
        super((String) scope.getParameter(IScope.NAMESTATIC));
        pannel = new JPanel();
        Map m;

        if (((String) scope.getParameter(IScope.NAMESTATIC)).equalsIgnoreCase("MEAN WORDS FOR SONG PER YEARS") ||
                ((String) scope.getParameter(IScope.NAMESTATIC)).equalsIgnoreCase("MEAN SONGS FOR ARTIST PER YEARS")  ) {
            m = ((Map<String,Double>) scope.getParameter(IScope.STATISTIC));
        }else {
            m = ((Map<String,Long>) scope.getParameter(IScope.STATISTIC));
        }


       // int lenght = m.size();
        //int count = 0;

        int y = 40;
        JLabel name;
        JLabel statistic;
        for (Object key : m.keySet()) {

            data = (String)key;
            name = new JLabel(data);
            if (((String) scope.getParameter(IScope.NAMESTATIC)).equalsIgnoreCase("MEAN WORDS FOR SONG PER YEARS") ||
                    ((String) scope.getParameter(IScope.NAMESTATIC)).equalsIgnoreCase("MEAN SONGS FOR ARTIST PER YEARS")  ) {
                numDouble = (Double) m.get(key);
                statistic = new JLabel(String.valueOf(numDouble));
            }
            else{
                numLong = (Long) m.get(key);
                statistic = new JLabel(String.valueOf(numLong));
            }
            pannel.add(name);
            pannel.add(statistic);

            name.setBounds(35, y, 130, 20);
            statistic.setBounds(170,y,130,20);
            y = y + 35;

            //count++;
        }
        back = new JButton("BACK");
        back.setBounds(75, y, 150, 20);

        setSize(350, y + 90);
        setLocation(500, 280);

        pannel.setLayout(null);
        pannel.add(back);

        getContentPane().add(pannel);
        getContentPane().add(pannel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

        back.addActionListener(e -> {
            Statistics statistics = new Statistics(scope);
            dispose();
        });

    }
}

