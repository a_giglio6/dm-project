package it.common.gui.generic;

import it.common.chain.IScope;
import it.common.gui.LoginPage;
import it.common.gui.admin.FindUserPage;
import it.common.gui.user.HomePageClient;
import it.common.model.Artist;
import it.dao.CRUDGenericManager;

import javax.swing.*;

public class Statistics extends JFrame {
    private JButton mostFavorArtist;
    private JButton mostFavoriteSongs;
    private JButton mostFavoriteSongsForGenre;
    private JButton mostFavoriteSongsForArtist;
    private JButton meanWordsForSongsPerYears;
    private JButton meanSongsForArtistPerYears;
    private JButton mostFavoriteGenres;
    private JButton mostUsedWordsForArtist;
    private JButton mostUsedWordsForGenre;
    private JPanel pannel;
    private JButton back;

    private JTextField genTosearch;
    private JTextField genTosearch1;
    private JTextField artistTosearch;
    private JTextField artistTosearch1;

    public Statistics(IScope scope) {
        super("SELECT OPERATION");
        pannel = new JPanel();

        mostFavoriteSongs = new JButton("MOST FAVORITE SONGS");
        mostFavoriteSongsForGenre = new JButton("MOST FAVORITE SONGS FOR GENRE");
        mostFavoriteSongsForArtist = new JButton("MOST FAVORITE SONGS FOR ARTIST");
        meanWordsForSongsPerYears = new JButton("MEAN WORDS FOR SONG PER YEARS");
        meanSongsForArtistPerYears = new JButton("MEAN SONGS FOR ARTIST PER YEARS");
        mostFavoriteGenres = new JButton("MOST FAVORITE GENRES");
        mostUsedWordsForArtist = new JButton("MOST USED WORDS FOR ARTIST");
        mostUsedWordsForGenre = new JButton("MOST USED WORDS FOR GENRE");
        mostFavorArtist = new JButton("MOST FAVORITE ARTISTS");

        genTosearch = new JTextField(15);
        genTosearch1 = new JTextField(15);
        artistTosearch = new JTextField(15);
        artistTosearch1 = new JTextField(15);

        back = new JButton("BACK");

        setSize(500, 500);
        setLocation(500, 280);
        pannel.setLayout(null);

        mostFavoriteSongs.setBounds(30, 75, 200, 20);
        mostFavoriteSongsForGenre.setBounds(30, 100, 250, 20);
        genTosearch.setBounds(315, 100, 150, 20);
        mostFavoriteSongsForArtist.setBounds(30, 125, 250, 20);
        artistTosearch1.setBounds(315, 125, 150, 20);
        meanWordsForSongsPerYears.setBounds(30, 150, 250, 20);
        meanSongsForArtistPerYears.setBounds(30, 175, 250, 20);
        mostFavoriteGenres.setBounds(30, 200, 200, 20);
        mostUsedWordsForArtist.setBounds(30, 225, 250, 20);
        artistTosearch.setBounds(315, 225, 150, 20);
        mostUsedWordsForGenre.setBounds(30, 250, 250, 20);
        genTosearch1.setBounds(315, 250, 150, 20);
        mostFavorArtist.setBounds(30, 275, 200, 20);

        back.setBounds(170, 330, 150, 20);

        pannel.add(mostFavoriteSongs);
        pannel.add(mostFavoriteSongsForGenre);
        pannel.add(genTosearch);
        pannel.add(genTosearch1);
        pannel.add(artistTosearch);
        pannel.add(artistTosearch1);
        pannel.add(mostFavoriteSongsForArtist);
        pannel.add(meanWordsForSongsPerYears);
        pannel.add(meanSongsForArtistPerYears);
        pannel.add(mostFavoriteGenres);
        pannel.add(mostUsedWordsForArtist);
        pannel.add(mostUsedWordsForGenre);
        pannel.add(mostFavorArtist);

        pannel.add(back);

        getContentPane().add(pannel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);


        mostFavoriteSongs.addActionListener(e -> {
            scope.addParameter(IScope.NAMESTATIC, mostFavoriteSongs.getText());
            scope.addParameter(IScope.STATISTIC, CRUDGenericManager.getStatistic(CRUDGenericManager.MOSTFAVORITESONGS));

            ShowStatistic showStatistic = new ShowStatistic(scope);
            dispose();

        });
        mostFavoriteSongsForGenre.addActionListener(e -> {
            scope.addParameter(IScope.NAMESTATIC, mostFavoriteSongsForGenre.getText());
            scope.addParameter(IScope.STATISTIC, CRUDGenericManager.getStatistic(genTosearch.getText(), CRUDGenericManager.MOSTFAVORITESONGSFORGENRE));

            ShowStatistic showStatistic = new ShowStatistic(scope);
            dispose();
        });

        mostFavoriteSongsForArtist.addActionListener(e -> {

            Artist a = (Artist) CRUDGenericManager.findMongo(artistTosearch1.getText());
            if (a == null) {
                JOptionPane.showMessageDialog(null, "Error Artist not found");
                Statistics statistics = new Statistics(scope);
                dispose();
            }

            scope.addParameter(IScope.NAMESTATIC, mostFavoriteSongsForArtist.getText());
            scope.addParameter(IScope.STATISTIC, CRUDGenericManager.getStatistic(a,CRUDGenericManager.MOSTFAVORITESONGSFORARTIST));

            ShowStatistic showStatistic = new ShowStatistic(scope);
            dispose();

        });
        meanWordsForSongsPerYears.addActionListener(e -> {
            scope.addParameter(IScope.NAMESTATIC, meanWordsForSongsPerYears.getText());
            scope.addParameter(IScope.STATISTIC, CRUDGenericManager.getStatistic(CRUDGenericManager.MEANWORDSFORSONGPERYEARS));

            ShowStatistic showStatistic = new ShowStatistic(scope);
            dispose();
        });

        meanSongsForArtistPerYears.addActionListener(e -> {
            scope.addParameter(IScope.NAMESTATIC, meanSongsForArtistPerYears.getText());
            scope.addParameter(IScope.STATISTIC, CRUDGenericManager.getStatistic(CRUDGenericManager.MEANSONGSFORARTISTPERYEARS));

            ShowStatistic showStatistic = new ShowStatistic(scope);
            dispose();

        });
        mostFavoriteGenres.addActionListener(e -> {
            scope.addParameter(IScope.NAMESTATIC, mostFavoriteGenres.getText());
            scope.addParameter(IScope.STATISTIC, CRUDGenericManager.getStatistic(CRUDGenericManager.MOSTFAVORITEGENRES));

            ShowStatistic showStatistic = new ShowStatistic(scope);
            dispose();
        });

        mostUsedWordsForArtist.addActionListener(e -> {
            Artist a = (Artist) CRUDGenericManager.findMongo(artistTosearch.getText());
            if (a == null) {
                JOptionPane.showMessageDialog(null, "Error Artist not found");
                Statistics statistics = new Statistics(scope);
                dispose();
            }
            scope.addParameter(IScope.NAMESTATIC, mostUsedWordsForArtist.getText());
            scope.addParameter(IScope.STATISTIC, CRUDGenericManager.getStatistic(a,CRUDGenericManager.MOSTUSEDWORDSFORARTIST));

            ShowStatistic showStatistic = new ShowStatistic(scope);
            dispose();

        });
        mostUsedWordsForGenre.addActionListener(e -> {
            scope.addParameter(IScope.NAMESTATIC, mostUsedWordsForGenre.getText());
            scope.addParameter(IScope.STATISTIC, CRUDGenericManager.getStatistic(genTosearch1.getText(), CRUDGenericManager.MOSTUSEDWORDSFORGENRE));

            ShowStatistic showStatistic = new ShowStatistic(scope);
            dispose();
        });

        mostFavorArtist.addActionListener(e -> {
            scope.addParameter(IScope.NAMESTATIC, mostFavorArtist.getText());
            scope.addParameter(IScope.STATISTIC, CRUDGenericManager.getStatistic(CRUDGenericManager.MOSTFAVORITEARTISTS));

            ShowStatistic showStatistic = new ShowStatistic(scope);
            dispose();

        });

        back.addActionListener(e -> {
            HomePageClient homePageClient = new HomePageClient(scope);
            dispose();
        });
    }
}