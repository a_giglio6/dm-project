package it.common.gui.generic;

import it.common.chain.IScope;
import it.common.gui.user.HomePageClient;
import it.common.model.Customer;
import it.common.model.Song;
import it.dao.CRUDGenericManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import weka.core.Attribute;
import weka.core.Instances;
import weka.core.converters.CSVLoader;
import weka.core.stemmers.SnowballStemmer;
import weka.core.stopwords.Rainbow;
import weka.core.tokenizers.NGramTokenizer;
import weka.filters.unsupervised.attribute.StringToWordVector;

import javax.swing.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class MoodRecommendation extends JFrame {
    private JPanel pannel;
    private JButton back;

    private static final Logger LOGGER = LogManager.getLogger(MoodRecommendation.class);

    private static final String RANDOM_DATASET_PATH = ".\\src\\main\\resources\\randomDataset.csv";


    public MoodRecommendation(IScope scope) throws Exception {
        super("MOOD RECOMMENDATION");
        pannel = new JPanel();
        JLabel song;

        Customer customer = (Customer) scope.getParameter(IScope.USER);

        List<Song> randomSelection = CRUDGenericManager.getRandomSongs(100, customer);
        Instances instances = toArff(randomSelection);
        StringToWordVector stringToWordVector = preProcess(instances);

        //TODO: CLASSIFICAZIONE CANZONI

        JButton[] show = new JButton[5];
        JButton[] add = new JButton[5];

        int i;
        int y = 40;
        JLabel title;

        for (i = 0; i < 10; i++){

            title = new JLabel();

            show[i] = new JButton("SHOW");
            add[i] = new JButton("ADD");

            show[i].setBounds(300, y, 80, 20);
            add[i].setBounds(400, y, 80, 20);
            y = y + 35;

            pannel.add(show[i]);
            pannel.add(add[i]);

            show[i].addActionListener(e -> {
                //scope.addParameter(IScope.SONG, song);
                ShowSong showSong = new ShowSong(scope);
                dispose();
            });

            add[i].addActionListener(e -> {
                try {
                    //customer.addPrefSong(song);
                    scope.addParameter(IScope.USER, customer);
                    CRUDGenericManager.writePref(customer);
                    JOptionPane.showMessageDialog(null, "SONG ADDED");
                } catch (Exception ex) {
                    LOGGER.error(ex);
                }
                dispose();
            });

        }

        back = new JButton("BACK");

        back.setBounds(135, y, 150, 20);
        pannel.add(back);

        setSize(450, y + 90);
        setLocation(500, 280);
        pannel.setLayout(null);

        getContentPane().add(pannel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

        back.addActionListener(e -> {
            HomePageClient homePageClient = new HomePageClient(scope);
            dispose();

        });

    }

    private Instances toArff(List<Song> randomSelection) throws IOException {
        File file = new File(RANDOM_DATASET_PATH);
        FileWriter fileWriter = new FileWriter(file);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        bufferedWriter.write("Title,text,mood");
        bufferedWriter.newLine();
        for (Song song : randomSelection){
            bufferedWriter.write(song.getName());
            bufferedWriter.write(",\"");
            String lyrics = song.getLyrics().replaceAll("\n", " ").replaceAll("\"", "'");
            bufferedWriter.write(lyrics);
            bufferedWriter.write("\",");
            bufferedWriter.newLine();
        }
        bufferedWriter.close();
        fileWriter.close();

        CSVLoader csvLoader = new CSVLoader();
        csvLoader.setStringAttributes("2");
        csvLoader.setSource(file);

        Instances instances =  csvLoader.getDataSet();
        //instances.setClassIndex(3);

        return instances;
    }

    public static StringToWordVector preProcess(Instances instances) throws Exception {
        NGramTokenizer nGramTokenizer = new NGramTokenizer();
        nGramTokenizer.setNGramMaxSize(2);
        nGramTokenizer.setDelimiters(".,;:'()?!/ -_<>%&#");

        Rainbow rainbow = new Rainbow();

        SnowballStemmer snowballStemmer = new SnowballStemmer();
        snowballStemmer.setStemmer("porter");

        StringToWordVector stringToWordVector = new StringToWordVector();
        stringToWordVector.setLowerCaseTokens(true);
        stringToWordVector.setTokenizer(nGramTokenizer);
        stringToWordVector.setStemmer(snowballStemmer);
        stringToWordVector.setStopwordsHandler(rainbow);
        stringToWordVector.setTFTransform(true);
        stringToWordVector.setIDFTransform(true);

        stringToWordVector.setInputFormat(instances);

        return stringToWordVector;
    }

}
