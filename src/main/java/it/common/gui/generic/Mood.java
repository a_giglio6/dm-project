package it.common.gui.generic;

import it.common.chain.IScope;
import it.common.gui.user.HomePageClient;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.io.IOException;

public class Mood extends JFrame {
    private JPanel pannel;
    private JButton back;

    private JButton angry;
    private JButton happy;
    private JButton sad;
    private JButton relaxed;

    private static final Logger LOGGER = LogManager.getLogger(Mood.class);

    public Mood(IScope scope) {
        super("SELECT MOOD");
        pannel = new JPanel();

        angry = new JButton("ANGRY");
        happy = new JButton("HAPPY");
        sad = new JButton("SAD");
        relaxed = new JButton("RELAXED");
        back = new JButton("BACK");

        setSize(500, 250);
        setLocation(500, 280);
        pannel.setLayout(null);

        angry.setBounds(150, 30, 200, 20);
        happy.setBounds(150, 60, 200, 20);
        sad.setBounds(150, 90, 200, 20);
        relaxed.setBounds(150, 120, 200, 20);
        back.setBounds(150, 180, 200, 20);

        pannel.add(angry);
        pannel.add(happy);
        pannel.add(sad);
        pannel.add(relaxed);

        angry.addActionListener(e -> {
            scope.addParameter(IScope.MOOD, "angry");
            try {
                MoodRecommendation moodRecommendation = new MoodRecommendation(scope);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            dispose();
        });

        happy.addActionListener(e -> {
            scope.addParameter(IScope.MOOD, "happy");
            try {
                MoodRecommendation moodRecommendation = new MoodRecommendation(scope);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            dispose();
        });

        sad.addActionListener(e -> {
            scope.addParameter(IScope.MOOD, "sad");
            try {
                MoodRecommendation moodRecommendation = new MoodRecommendation(scope);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            dispose();
        });

        relaxed.addActionListener(e -> {
            scope.addParameter(IScope.MOOD, "angry");
            try {
                MoodRecommendation moodRecommendation = new MoodRecommendation(scope);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            dispose();
        });

        back.addActionListener(e -> {
            HomePageClient homePageClient = new HomePageClient(scope);
            dispose();
        });

        getContentPane().add(pannel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

    }
}
