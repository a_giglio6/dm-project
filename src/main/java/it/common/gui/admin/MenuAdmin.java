package it.common.gui.admin;

import it.common.chain.IScope;
import it.common.gui.LoginPage;
import it.common.gui.generic.FindPage;

import javax.swing.*;

public class MenuAdmin extends JFrame {
    private JPanel pannel;
    private JButton user;
    private JButton artistAndSong;
    private JButton back;

    public MenuAdmin(IScope scope) {
        super("SELECT OPERATION");
        pannel = new JPanel();
        user = new JButton("MODIFY USER");
        artistAndSong = new JButton("MODIFY ARTIST & SONG");
        back = new JButton("BACK");

        setSize(300, 200);
        setLocation(500, 280);
        pannel.setLayout(null);

        user.setBounds(70, 40, 150, 20);
        artistAndSong.setBounds(50, 75, 200, 20);
        back.setBounds(70, 110, 150, 20);

        pannel.add(user);
        pannel.add(artistAndSong);
        pannel.add(back);

        getContentPane().add(pannel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

        artistAndSong.addActionListener(e -> {
            FindPage findPage = new FindPage(scope);
            dispose();
        });

        user.addActionListener(e -> {
            FindUserPage findUserPage = new FindUserPage(scope);
            dispose();

        });

        back.addActionListener(e -> {
            LoginPage loginPage = new LoginPage(scope);
            dispose();

        });
    }
}
