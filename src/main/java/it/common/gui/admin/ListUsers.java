package it.common.gui.admin;

import it.common.chain.IScope;
import it.common.gui.generic.ShowUser;
import it.common.model.Customer;
import it.dao.CRUDGenericManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.types.ObjectId;

import javax.swing.*;
import java.util.Map;

public class ListUsers extends JFrame {
    private JPanel pannel;
    private JButton back;
    private String firstAndLastNameCustomer;
    private static final Logger LOGGER = LogManager.getLogger(ListUsers.class);


    public ListUsers(IScope scope) {
        super("LIST USERS");
        pannel = new JPanel();

        Map<ObjectId, Customer> customers = ((Map) scope.getParameter(IScope.CUSTOMERS));

        int lenght = customers.size();
        JButton[] show = new JButton[lenght];
        JButton[] delete = new JButton[lenght];

        int count = 0;
        int y = 40;
        JLabel name;
        for (Customer c : customers.values()) {

            firstAndLastNameCustomer = c.getFirstName() + " " + c.getLastName();
            name = new JLabel(firstAndLastNameCustomer);

            show[count] = new JButton("SHOW");
            delete[count] = new JButton("DELETE");
            pannel.add(delete[count]);
            pannel.add(show[count]);
            pannel.add(name);

            delete[count].setBounds(175, y, 90, 20);
            show[count].setBounds(280, y, 90, 20);
            name.setBounds(35, y, 130, 20);
            y = y + 35;
            show[count].addActionListener(e -> {
                scope.addParameter(IScope.CUSTOMER, c);
                ShowUser showUser = new ShowUser(scope);
                dispose();
            });
            delete[count].addActionListener(e -> {
                try {
                    if ((boolean) CRUDGenericManager.deleteMongo(c)) {
                        customers.remove(c.getCustId());
                        CRUDGenericManager.deleteNeo4j(c, c.getCustId().toString());
                        scope.modifyParameter(IScope.CUSTOMER, customers);
                        JOptionPane.showMessageDialog(null, "USER DELETE");
                    } else {
                        JOptionPane.showMessageDialog(null, "Error when try to delete");
                    }
                    ListUsers listUsers = new ListUsers(scope);
                    dispose();

                } catch (Exception ex) {
                    LOGGER.error(ex);
                }
            });
            count++;
        }
        back = new JButton("BACK");

        back.setBounds(135, y, 150, 20);

        setSize(400, y + 90);
        setLocation(500, 280);
        pannel.setLayout(null);

        pannel.add(back);

        getContentPane().add(pannel);
        getContentPane().add(pannel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

        back.addActionListener(e -> {
            FindUserPage findUserPage = new FindUserPage(scope);
            dispose();
        });

    }

}