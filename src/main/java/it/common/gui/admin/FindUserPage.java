package it.common.gui.admin;

import it.common.chain.IScope;
import it.common.gui.LoginPage;
import it.common.model.Customer;
import it.dao.CRUDGenericManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import java.util.Map;

public class FindUserPage extends JFrame {
    private JPanel pannel;
    private JTextField fistname;
    private JTextField lastname;
    private JButton back;
    private JButton find;
    private JLabel firstNameLable;
    private JLabel lastNameLable;

    private static final String USER = "USER";
    private static final String SUPERUSER = "SUPERUSER";

    private static final Logger LOGGER = LogManager.getLogger(LoginPage.class);

    public FindUserPage(IScope scope) {
        super("FIND USER");

        pannel = new JPanel();

        fistname = new JTextField(15);
        lastname = new JTextField(15);

        firstNameLable = new JLabel("First Name - ");
        lastNameLable = new JLabel("Last Name - ");

        find = new JButton("FIND");
        back = new JButton("BACK");

        setSize(270, 230);
        setLocation(500, 280);
        pannel.setLayout(null);


        firstNameLable.setBounds(20, 28, 80, 20);
        lastNameLable.setBounds(20, 63, 80, 20);
        fistname.setBounds(115, 30, 115, 20);
        lastname.setBounds(115, 65, 115, 20);

        back.setBounds(30, 100, 80, 20);
        find.setBounds(130, 100, 80, 20);

        pannel.add(firstNameLable);
        pannel.add(lastNameLable);
        pannel.add(fistname);
        pannel.add(lastname);
        pannel.add(back);
        pannel.add(find);

        getContentPane().add(pannel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

        find.addActionListener(e -> {
            try {
                String fName = fistname.getText();
                String lName = lastname.getText();

                if (fistname.equals("") && lastname.equals("")) {
                    JOptionPane.showMessageDialog(null, "Please insert one element");
                } else {
                    Customer c = new Customer();
                    if (!fName.equals("")) {
                        c.setFirstName(fName);
                    }
                    if (!lName.equals("")) {
                        c.setLastName(lName);
                    }
                    Map map = (Map) CRUDGenericManager.findMongo(c);

                    if (map != null) {
                        scope.addParameter(IScope.CUSTOMERS, map);
                        ListUsers listUsers = new ListUsers(scope);
                        dispose();

                    } else {
                        JOptionPane.showMessageDialog(null, "user not found");
                    }
                }
            } catch (Exception ex) {
                LOGGER.error(ex);
            }
        });

        back.addActionListener(e -> {
            MenuAdmin menuAdmin = new MenuAdmin(scope);
            dispose();
        });
    }
}