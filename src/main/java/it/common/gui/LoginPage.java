package it.common.gui;

import it.common.chain.IScope;

import it.common.gui.admin.MenuAdmin;
import it.common.gui.user.HomePageClient;
import it.common.interfa.Client;
import it.common.model.Customer;
import it.common.model.Login;
import it.dao.CRUDGenericManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;

public class LoginPage extends JFrame {
    private JButton blogin;
    private JPanel loginpanel;
    private JTextField txuser;
    private JTextField pass;
    private JButton newUSer;
    private JLabel username;
    private JLabel password;
    private static final String USER = "USER";
    private static final String SUPERUSER = "SUPERUSER";

    private static final Logger LOGGER = LogManager.getLogger(LoginPage.class);

    public LoginPage(IScope scope) {
        super("Login Authentication");

        blogin = new JButton("LOGIN");
        loginpanel = new JPanel();
        txuser = new JTextField(15);
        pass = new JPasswordField(15);
        newUSer = new JButton("SIGN UP");
        username = new JLabel("User - ");
        password = new JLabel("Pass - ");

        setSize(300, 230);
        setLocation(500, 280);
        loginpanel.setLayout(null);


        txuser.setBounds(70, 30, 150, 20);
        pass.setBounds(70, 65, 150, 20);
        blogin.setBounds(110, 100, 80, 20);
        newUSer.setBounds(110, 135, 80, 20);
        username.setBounds(20, 28, 80, 20);
        password.setBounds(20, 63, 80, 20);

        loginpanel.add(blogin);
        loginpanel.add(txuser);
        loginpanel.add(pass);
        loginpanel.add(newUSer);
        loginpanel.add(username);
        loginpanel.add(password);

        getContentPane().add(loginpanel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

        blogin.addActionListener(e -> {
            try {

                String usertxt = " ";
                String passtxt = " ";
                String puname = txuser.getText();
                String ppaswd = pass.getText();

                if (!puname.equals(usertxt) && !ppaswd.equals(passtxt)) {
                    Customer customer = (Customer) getClient(scope, puname, ppaswd);
                    if (customer != null) {
                        scope.addParameter(IScope.USER, customer);
                        if (customer.getLogin().getAuthorization().equals(SUPERUSER)) {
                            MenuAdmin menuAdmin = new MenuAdmin(scope);
                            dispose();
                        } else if (customer.getLogin().getAuthorization().equals(USER)) {
                            HomePageClient homePageClient = new HomePageClient(scope);
                            dispose();
                        } else {
                            LOGGER.error("unknown type user");
                            JOptionPane.showMessageDialog(null, "Wrong Username / Password");
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "Wrong Username / Password");
                        txuser.setText("");
                        pass.setText("");
                        txuser.requestFocus();
                    }
                } else if (puname.equals("") && ppaswd.equals("")) {
                    JOptionPane.showMessageDialog(null, "Please insert Username and Password");
                } else {
                    JOptionPane.showMessageDialog(null, "Wrong Username / Password");
                    txuser.setText("");
                    pass.setText("");
                    txuser.requestFocus();
                }
            } catch (Exception d) {
                d.printStackTrace();
            }

        });

        newUSer.addActionListener(e -> {
            NewUser user = new NewUser(scope);
            dispose();

        });
    }

    public Client getClient(IScope scope, String user, String psw) {
        Customer customer = new Customer();
        Login login = new Login();
        login.setUser(user);
        login.setPassword(psw);
        customer.setLogin(login);
        try {
            customer = (Customer) CRUDGenericManager.findMongo(customer);

        } catch (Exception e) {
            LOGGER.error(e);
            return null;
        }

        return customer;
    }
}