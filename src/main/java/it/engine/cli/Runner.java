package it.engine.cli;

import it.common.chain.AbsOpChain;
import it.common.chain.Scope;
import it.engine.chain.EngineOpChain;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import picocli.CommandLine;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

@CommandLine.Command(name = "java -jar task2-3.jar", mixinStandardHelpOptions = true, version = "1.0.0")
public class Runner implements Callable<Void> {

    private final static Logger LOGGER = LogManager.getLogger(Runner.class);

    @CommandLine.Option(names = { "-v", "--verbose" }, description = "Verbose mode. Helpful for troubleshooting. " + "Multiple -v options increase the verbosity.")
    private boolean[] verbose = new boolean[0];

    @CommandLine.Option(names = { "-db", "--dbconn" }, arity = "1", description = "DB to connect to.")
    private String dbConn;

    @CommandLine.Option(names = { "-dbn", "--dbname" }, arity = "1", description = "DB name to connect to.")
    private String dbName;

    @CommandLine.Option(names = { "-njcon", "--neo4jConn" }, arity = "1", description = "DB to connect to.")
    private String njcon;

    @CommandLine.Option(names = { "-nju", "--neo4jUser" }, arity = "1", description = "DB name to connect to.")
    private String nju;

    @CommandLine.Option(names = { "-njp", "--neo4jPass" }, arity = "1", description = "DB name to connect to.")
    private String njp;

    @CommandLine.Option(names = { "-us", "--user" }, arity = "1", description = "user to connect on JPA")
    private String dbUser;

    @CommandLine.Option(names = { "-ps", "--password" }, arity = "1", description = "password to connect on JPA")
    private String dbPass;

    @CommandLine.Option(names = { "-m", "--mock" }, arity = "0", description = "Use mock mode, default: ${DEFAULT-VALUE}")
    private boolean useMock = false;

    @Override
    public Void call() {
        try {

            LOGGER.info("DBCONN: [{}]", dbConn);
            LOGGER.info("MOCK: [{}]", useMock);
            LOGGER.info("DBNAME: [{}]", dbName);
            LOGGER.info("DBNAME: [{}]", dbName);
            LOGGER.info("DBPASS: [{}]", dbPass);
            LOGGER.info("NEO4JUSER: [{}]", nju);
            LOGGER.info("NEO4JPASS: [{}]", njp);
            LOGGER.info("NEO4JCONN: [{}]", njcon);

            Map<String, String> initParams = new HashMap<String, String>();
            initParams.put(Scope.DBCONN, dbConn);
            initParams.put(Scope.DBNAME,dbName);
            initParams.put(Scope.USER,dbUser);
            initParams.put(Scope.PASSWORD,dbPass);
            //initParams.put("MOCK", String.valueOf(useMock));
            initParams.put(Scope.NEOCONN, njcon);
            initParams.put(Scope.NEOUSER, nju);
            initParams.put(Scope.NEOPASS, njp);

            LOGGER.info("Avvio elaborazione chain");

            AbsOpChain chain = new EngineOpChain();
            chain.execChain(initParams);

            LOGGER.info("Elaborazione chain completata con successo");
            System.exit(0);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            System.exit(1);
        }
        return null;
    }

    public static void main(String[] args) {

        CommandLine.call(new Runner(), args);
    }

}
