package it.engine.chain.operation;

import it.common.chain.AbsOperation;
import it.common.chain.IScope;
import it.common.chain.Scope;
import it.common.gui.thread.GuiThread;
import it.dao.CRUDGenericManager;

import java.util.Map;
import java.util.concurrent.TimeUnit;

public class StartOp extends AbsOperation {
    @Override
    public void handleExec(IScope scope) throws Exception {

        setInitOnscope(scope);
        CRUDGenericManager.setUpManager(scope);

        GuiThread guiThread = new GuiThread(scope);
        guiThread.run();

        while (!guiThread.isAlive()){
            TimeUnit.SECONDS.sleep(1);
        };

    }

    private void setInitOnscope(IScope scope){
        Map<String,String> initParams =getInitParams(scope);
        scope.addParameter(Scope.DBNAME,initParams.get(Scope.DBNAME));
        scope.addParameter(Scope.USER,initParams.get(Scope.USER));
        scope.addParameter(Scope.PASSWORD,initParams.get(Scope.PASSWORD));
        scope.addParameter(Scope.DBCONN,initParams.get(Scope.DBCONN));
        scope.addParameter(Scope.NEOCONN,initParams.get(Scope.NEOCONN));
        scope.addParameter(Scope.NEOPASS,initParams.get(Scope.NEOPASS));
        scope.addParameter(Scope.NEOUSER,initParams.get(Scope.NEOUSER));

    }
}
