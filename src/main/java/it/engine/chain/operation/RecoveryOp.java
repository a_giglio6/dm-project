package it.engine.chain.operation;

import it.common.chain.IScope;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class RecoveryOp implements it.common.chain.IOperation {

    private static final Logger LOGGER = LogManager.getLogger(RecoveryOp.class);

    private static final String STARTING_DATASET_PATH = ".\\src\\main\\resources\\startingDataset.xls";
    private static final String DATASET_PATH = ".\\src\\main\\resources\\dataset.csv";

    public static final String SONG_LYRICS = "http://www.songlyrics.com/index.php?section=search&searchW=";

    @Override
    public void handleExec(IScope scope) throws Exception {

        lyricsRecovery(STARTING_DATASET_PATH);
        lyricsFiltering(STARTING_DATASET_PATH);
        toCSV(STARTING_DATASET_PATH, DATASET_PATH);

    }

    public static void lyricsRecovery(String filePath) throws IOException, InvalidFormatException, InterruptedException {

        try {
            FileInputStream inputStream = new FileInputStream(new File(filePath));
            Workbook workbook = WorkbookFactory.create(inputStream);
            DataFormatter dataFormatter = new DataFormatter();
            Sheet sheet = workbook.getSheetAt(0);

            int i = 1;
            for (Row row : sheet) {
                if (row.getRowNum() == 0) {
                    Cell labelCell = row.getCell(4);
                    labelCell.setCellValue("Lyrics");
                }
                LOGGER.info("Recovering lyrics for song in row {}/{}...", i, sheet.getLastRowNum() - 1);

                Cell artistCell = row.getCell(2);
                String artist = dataFormatter.formatCellValue(artistCell);
                artist = artist.replace("_", " ");
                Cell titleCell = row.getCell(3);
                String title = dataFormatter.formatCellValue(titleCell);
                title = title.replace("_", " ");
                Cell lyricsCell = row.getCell(4);
                String lyrics = dataFormatter.formatCellValue(lyricsCell);
                if (lyrics.isEmpty()){
                    lyrics = songLyrics(artist, title);
                    lyricsCell.setCellValue(lyrics);
                }

                i++;
            }
            inputStream.close();

            FileOutputStream outputStream = new FileOutputStream(filePath);
            workbook.write(outputStream);
            workbook.close();
            outputStream.close();
        } catch (Exception e){
            LOGGER.info("Exception occurred: {}", e.toString());
        }

    }

    public static void lyricsFiltering(String filePath) {

        try {
            FileInputStream inputStream = new FileInputStream(new File(filePath));
            Workbook workbook = WorkbookFactory.create(inputStream);
            DataFormatter dataFormatter = new DataFormatter();
            Sheet sheet = workbook.getSheetAt(0);
            List<Row> rows = new ArrayList<>();

            for (Row row : sheet) {
                Cell lyricsCell = row.getCell(1);
                String lyrics = dataFormatter.formatCellValue(lyricsCell);
                if (lyrics.isEmpty() || lyrics.contains("We do not have the lyrics for")){
                    rows.add(row);
                }
            }
            for (Row row : rows){
                sheet.removeRow(row);
            }
            inputStream.close();

            FileOutputStream outputStream = new FileOutputStream(filePath);
            workbook.write(outputStream);
            workbook.close();
            outputStream.close();
        } catch (Exception e){
            LOGGER.info("Exception occurred: {}", e.toString());
        }

    }

    public static String songLyrics(String artist, String title) throws InterruptedException {
        String lyrics = "";
        String url = SONG_LYRICS + artist + " " + title;

        LOGGER.info("Recovering lyrics link for {} - {} ... ", artist, title);

        try {
            Connection connection = Jsoup.connect(url).timeout(0);
            Document document = connection.get();
            LOGGER.info("Response = {}", connection.response().statusCode());

            Element element = document.selectFirst("div[class='serpresult']").selectFirst("a[href]");
            String link = element.attr("href");
            lyrics = songLyrics2(link);
        } catch (Exception e) {
            LOGGER.info("Exception occurred: {}", e.toString());
        }
        TimeUnit.SECONDS.sleep(1);

        return lyrics;
    }

    public static String songLyrics2(String link) throws InterruptedException {
        String lyrics = "";

        LOGGER.info("Recovering lyrics for song at {} ...", link);

        try {
            Connection connection = Jsoup.connect(link).timeout(0);
            Document document = connection.get();
            LOGGER.info("Response = {}", connection.response().statusCode());

            lyrics = document.select("p[id='songLyricsDiv']").first().text();
            LOGGER.info("Lyrics added");
        } catch (Exception e) {
            LOGGER.info("Exception occurred: {}", e.toString());
        }
        TimeUnit.SECONDS.sleep(1);

        return lyrics;
    }

    public static void toCSV(String filePath, String targetPath){

        try{
            FileInputStream inputStream = new FileInputStream(new File(filePath));
            Workbook workbook = WorkbookFactory.create(inputStream);
            DataFormatter dataFormatter = new DataFormatter();
            Sheet sheet = workbook.getSheetAt(0);

            FileWriter fileWriter = new FileWriter(targetPath);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            for (Row row : sheet) {
                Cell classCell = row.getCell(0);
                String classes = dataFormatter.formatCellValue(classCell);
                Cell lyricsCell = row.getCell(3);
                String lyrics = dataFormatter.formatCellValue(lyricsCell);

                bufferedWriter.write(classes + "," + lyrics);
                bufferedWriter.newLine();
            }

            bufferedWriter.close();
            fileWriter.close();
        }
        catch (Exception e){
            LOGGER.info("Exception occurred: {}", e.toString());
        }

    }

}
