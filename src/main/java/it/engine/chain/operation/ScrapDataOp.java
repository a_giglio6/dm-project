package it.engine.chain.operation;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.LinkedTreeMap;
import it.common.chain.AbsOperation;
import it.common.chain.IScope;
import it.common.chain.Scope;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class ScrapDataOp extends AbsOperation {

    private static final String LYRICS_URL = "https://www.lyrics.com/";

    private static final String[] discogsQuery = {"https://api.discogs.com/database/search?q=", "&key=", "&secret="};
    private static final String discogsClientId = "TuIqieCbHmIhbLfBdCCo";
    private static final String discogsClientSecret = "aduQISRJACmszKvWlSDmbyiGWPisaItG";

    private static final String OUTPUT_PATH = "/Users/alegi/Documents/task2-3/src/main/java/it/dbFile.txt";
    private static final String TEMP_PATH = "/Users/alegi/Documents/task2-3/src/main/java/it/File.txt";

    // size limit in MB
    private static final int fileSizeLimit = 200;

    private static final Logger LOGGER = LogManager.getLogger(ScrapDataOp.class);

    @Override
    public void handleExec(IScope scope) throws Exception {
        setInitOnscope(scope);

        HashMap<String, HashMap> finalMap = lyricsScraping(TEMP_PATH);
        scope.addParameter("dbMap", finalMap);

        prettyFile(TEMP_PATH, OUTPUT_PATH);
        scope.addParameter("filePath", OUTPUT_PATH);
    }

    public static HashMap<String, HashMap> lyricsScraping(String filePath) throws IOException, InterruptedException {

        String url = LYRICS_URL + "/topartists.php";
        HashMap<String, HashMap> map = new HashMap<>();

        Set artistsLinks = getLyricsArtists(url);
        for (Object artistLink : artistsLinks) {
            HashMap<String, HashMap> artistMap = new HashMap<>();
            Set albumsLinks = getLyricsAlbums((String) artistLink);
            for (Object albumLink : albumsLinks) {
                Set songsLinks = getLyricsSongs((String) albumLink);
                if (songsLinks.size() == 1) {
                    LOGGER.info("Album discarded: inadequate number songs");
                    continue;
                }
                HashMap<String, HashMap> albumMap = getAlbumMap(songsLinks);
                if (albumMap == null || albumMap.size() < 2) {
                    LOGGER.info("Album discarded: invalid album");
                    continue;
                }
                String albumName = getAlbumName(albumLink);
                artistMap.put(albumName, albumMap);
            }
            String artistName = getArtistName(artistLink);
            map.put(artistName, artistMap);
            HashMap<String, HashMap> tempMap = new HashMap<>();
            tempMap.put(artistName, artistMap);
            int fileSize = mapToFile(tempMap, filePath);
            if (fileSize > fileSizeLimit) {
                LOGGER.info("Size limit: program will be ended");
                break;
            }
        }

        return map;
    }

    private static int mapToFile(HashMap<String, HashMap> map, String filePath) {
        File file = new File(filePath);
        FileWriter fr = null;
        BufferedWriter br = null;
        LOGGER.info("Writing artist document to file");
        try {
            if (file.exists()) {
                fr = new FileWriter(file, true);
            } else {
                fr = new FileWriter(file);
            }
            br = new BufferedWriter(fr);
            GsonBuilder gsonBuilder = new GsonBuilder().disableHtmlEscaping();
            Gson gson = gsonBuilder.setPrettyPrinting().create();
            String albumGson = gson.toJson(map);
            br.write(albumGson);
            br.newLine();
            LOGGER.info("Document added");
        } catch (IOException e) {
            e.printStackTrace();
            LOGGER.info(e);
        } finally {
            try {
                br.close();
                fr.close();
                LOGGER.info("File closed");
            } catch (IOException e) {
                e.printStackTrace();
                LOGGER.info(e);
            }
        }
        return (int) (file.length() / (1024 * 1024));
    }

    public static String getArtistName(Object artistLink) throws InterruptedException, IOException {
        String artistName = null;
        LOGGER.info("Obtaining artist name for artist at {}...", artistLink);
        try {
            Connection connection = Jsoup.connect((String) artistLink);
            Document doc = connection.get();
            LOGGER.info("Response = {}", connection.response().statusCode());
            artistName = doc.select("h1[class='artist']").text();
            TimeUnit.SECONDS.sleep(1);
            LOGGER.info("Name obtained");
        } catch (IOException e) {
            LOGGER.info(e);
        }
        return artistName;
    }

    public static String getAlbumName(Object albumLink) throws IOException, InterruptedException {
        String albumName = null;
        LOGGER.info("Obtaining album name for album at {}...", albumLink);
        try {
            Connection connection = Jsoup.connect((String) albumLink);
            Document doc = connection.get();
            LOGGER.info("Response = {}", connection.response().statusCode());
            albumName = doc.select("h1").text();
            TimeUnit.SECONDS.sleep(1);
            LOGGER.info("Name obtained");
        } catch (IOException e) {
            LOGGER.info(e);
        }
        return albumName;
    }

    public static HashMap<String, HashMap> getAlbumMap(Set songsLinks) throws InterruptedException, IOException {
        HashMap<String, HashMap> albumMap = new HashMap<>();
        String artistTest = null;

        LOGGER.info("Building map for an album...");
        for (Object songLink : songsLinks) {
            LOGGER.info("Building map for song at {}", songLink);
            try {
                Connection connection = Jsoup.connect((String) songLink).timeout(0);
                Document doc = connection.get();
                LOGGER.info("Response = {}", connection.response().statusCode());
                Elements artistsElem = doc.select("h3[class='lyric-artist']").select("a[href^='artist']");
                String artist = artistsElem.first().text();
                if (artistTest == null) {
                    artistTest = artist;
                }
                String featuring = "";
                artistsElem.remove(0);
                for (Element element : artistsElem) {
                    featuring += element.text();
                    if (!(element == artistsElem.last())) {
                        featuring += ", ";
                    }
                }
                if (!artist.toLowerCase().equals(artistTest.toLowerCase())) {
                    LOGGER.info("Song discarded: different artists detected (invalid album)");
                    return null;
                }
                String title = doc.select("h1[class='lyric-title']").text().replace("#", "");
                String album = doc.select("a[href^='/album']").text().replace("#", "");
                String lyrics = doc.select("pre[id='lyric-body-text']").text().replace("#", "");
                if (!album.isEmpty()) {
                    HashMap songMap = new HashMap<>();
                    songMap.put("title", title);
                    songMap.put("lyrics", lyrics);
                    songMap.put("featuring", featuring);
                    songMap = getDiscogsSongDetail(title, artist, album, songMap);
                    if ((songMap.containsKey("genre") && songMap.containsKey("year"))) {
                        albumMap.put(title, songMap);
                        LOGGER.info("Song added to map");
                    } else {
                        LOGGER.info("Song discarded: not enough details obtained");
                        continue;
                    }
                }
                TimeUnit.SECONDS.sleep(1);
                LOGGER.info("Album map built");
            } catch (IOException e) {
                LOGGER.info(e);
            }
        }

        return albumMap;
    }

    public static HashMap getDiscogsSongDetail(String title, String artist, String album, HashMap song) throws IOException, InterruptedException {

        LOGGER.info("Obtaining details of {} - {}", title, artist);
        try {
            Connection connection = Jsoup.connect(discogsQuery[0] + title + " " + artist + " " + album
                    + discogsQuery[1] + discogsClientId
                    + discogsQuery[2] + discogsClientSecret)
                    .ignoreContentType(true);
            Document doc = connection.get();
            LOGGER.info("Response = {}", connection.response().statusCode());
            JSONObject jsonObject = new JSONObject(doc.text());
            JSONArray jsonArray = jsonObject.getJSONArray("results");
            if (!jsonArray.isEmpty()) {
                JSONObject result1 = jsonArray.getJSONObject(0);
                if (result1.has("genre") && result1.has("year")) {
                    String genre = jsonArray.getJSONObject(0).getJSONArray("genre").getString(0);
                    String year = jsonArray.getJSONObject(0).getString("year");
                    song.put("genre", genre);
                    song.put("year", year);
                }
            }
        } catch (IOException e) {
            LOGGER.info(e);
        }
        TimeUnit.SECONDS.sleep(1);
        LOGGER.info("Details added if present");

        return song;
    }

    public static Set getLyricsSongs(String albumLink) throws IOException, InterruptedException {
        Set<String> songsLinks = new HashSet<>();
        LOGGER.info("Scraping songs urls of album at {}", albumLink);
        try {
            Connection connection = Jsoup.connect(albumLink);
            Document doc = connection.timeout(0).get();
            LOGGER.info("Response = {}", connection.response().statusCode());
            Elements songsElem = doc.select("td[class='tal qx fsl']").select("div").select("a");
            for (Element songElem : songsElem) {
                songsLinks.add(LYRICS_URL + songElem.attr("href"));
            }
            TimeUnit.SECONDS.sleep(1);
            LOGGER.info("Songs added");
        } catch (IOException e) {
            LOGGER.info(e);
        }
        return songsLinks;
    }

    public static Set getLyricsAlbums(String artistLink) throws IOException, InterruptedException {
        Set<String> albumsLinks = new HashSet<>();
        LOGGER.info("Scraping albums urls for artist at {}", artistLink);
        try {
            Connection connection = Jsoup.connect(artistLink);
            Document doc = connection.get();
            LOGGER.info("Response = {}", connection.response().statusCode());
            Elements albumsElem = doc.select("div[class='tdata-ext']").select("div[class='clearfix']");
            for (Element albumElem : albumsElem) {
                int nSongs = albumElem.select("td[class='tal qx']").size();
                if (nSongs > 1) {
                    Element albumName = albumElem.select("h3[class='artist-album-label']").select("a").first();
                    albumsLinks.add(LYRICS_URL + albumName.attr("href"));
                }
            }
            TimeUnit.SECONDS.sleep(1);
            LOGGER.info("Albums added");
        } catch (IOException e) {
            LOGGER.info(e);
        }
        return albumsLinks;
    }

    public static Set getLyricsArtists(String url) throws IOException, InterruptedException {
        Set<String> artistsLinks = new HashSet<>();
        LOGGER.info("Artists urls scraping...");
        try {
            Connection connection = Jsoup.connect(url);
            Document doc = connection.get();
            LOGGER.info("Response = {}", connection.response().statusCode());
            Elements artistsElem = doc.select("td[class='tal qx']").select("a[href]");
            for (Element artistElem : artistsElem) {
                artistsLinks.add(LYRICS_URL + artistElem.attr("href"));
            }
            TimeUnit.SECONDS.sleep(1);
            LOGGER.info("Artists added");
        } catch (IOException e) {
            LOGGER.info(e);
        }
        return artistsLinks;
    }

    public void setInitOnscope(IScope scope) {
        Map<String, String> initParams = getInitParams(scope);
        scope.addParameter(Scope.DBNAME, initParams.get(Scope.DBNAME));
        scope.addParameter(Scope.USER, initParams.get(Scope.USER));
        scope.addParameter(Scope.PASSWORD, initParams.get(Scope.PASSWORD));
        scope.addParameter(Scope.DBCONN, initParams.get(Scope.DBCONN));
    }

    public static void prettyFile(String input, String output) throws IOException {
        File inputFile = new File(input);
        File tempFile = new File(output);

        BufferedReader reader = new BufferedReader(new FileReader(inputFile));
        BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));

        String currentLine;
        boolean flag1 = false;

        while ((currentLine = reader.readLine()) != null) {
            if (currentLine.equals("{")) {
                if (flag1) {
                    writer.write("\n  },");
                    continue;
                } else {
                    writer.write(currentLine);
                    flag1 = true;
                    continue;
                }
            }
            if (currentLine.equals("}")) {
                continue;
            }
            if (currentLine.equals("  }")) {
                continue;
            }
            writer.write("\n" + currentLine);
        }
        writer.write("\n  }\n}");
        writer.close();
        reader.close();

    }

    public static Set genreList(HashMap hashMap) {
        Set genres = new HashSet();

        for (Object artist : hashMap.keySet()) {
            LinkedTreeMap linkedTreeMap = (LinkedTreeMap) hashMap.get(artist);
            HashMap artistMap = gsonMapToHashMap(linkedTreeMap);
            for (Object album : artistMap.keySet()) {
                LinkedTreeMap linkedTreeMap1 = (LinkedTreeMap) artistMap.get(album);
                HashMap albumMap = gsonMapToHashMap(linkedTreeMap1);
                for (Object song : albumMap.keySet()) {
                    LinkedTreeMap linkedTreeMap2 = (LinkedTreeMap) albumMap.get(song);
                    HashMap songMap = gsonMapToHashMap(linkedTreeMap2);
                    String genre = (String) songMap.get("genre");
                    genres.add(genre);
                }
            }
        }

        return genres;
    }

    public static HashMap gsonMapToHashMap(LinkedTreeMap linkedTreeMap) {
        Gson gson = new Gson();

        String json = gson.toJson(linkedTreeMap);
        HashMap hashMap = gson.fromJson(json, HashMap.class);

        return hashMap;
    }

    public static HashMap fixFeaturing(HashMap hashMap) {
        int n=0;

        for (Object artist : hashMap.keySet()) {
            LinkedTreeMap linkedTreeMap = (LinkedTreeMap) hashMap.get(artist);
            HashMap artistMap = gsonMapToHashMap(linkedTreeMap);
            for (Object album : artistMap.keySet()) {
                LinkedTreeMap linkedTreeMap1 = (LinkedTreeMap) artistMap.get(album);
                HashMap albumMap = gsonMapToHashMap(linkedTreeMap1);
                for (Object song : albumMap.keySet()) {
                    LinkedTreeMap linkedTreeMap2 = (LinkedTreeMap) albumMap.get(song);
                    HashMap songMap = gsonMapToHashMap(linkedTreeMap2);

                    String lyrics = (String) songMap.get("lyrics");
                    String originalLine = lyrics.split("\n")[0];
                    String firstLine = originalLine.toLowerCase()
                            .replace("(", "")
                            .replace(")", "")
                            .replace("[", "")
                            .replace("]", "")
                            .replace("feat. ", "ft. ")
                            .replace("featuring ", "ft. ");
                    String featuring = "";
                    if (firstLine.contains("ft.")) {
                        n++;
                        if (firstLine.split(" ft. ").length > 1) {
                            firstLine = firstLine.split(" ft. ")[1];
                            firstLine = firstLine.split(" - ")[0];
                        } else {
                            firstLine = firstLine.split("ft. ")[1];
                            firstLine = firstLine.split(" - ")[0];
                        }
                        if (firstLine.split(" & ").length > 1) {
                            String firstLine1 = firstLine.split(" & ")[0];
                            String firstLine2 = firstLine.split(" & ")[1];
                            featuring += firstLine1;
                            featuring += ", " + firstLine2;
                        } else {
                            featuring = firstLine;
                        }
                        lyrics = lyrics.replace(originalLine + "\n", "");
                        lyrics = lyrics.replace(originalLine, "");
                        songMap.put("featuring", featuring);
                        songMap.put("lyrics", lyrics);
                        albumMap.put(song, songMap);
                        artistMap.put(album, albumMap);
                        hashMap.put(artist, artistMap);
                    }
                }
            }
        }
        return hashMap;
    }

    public static int artistToFile(String artist, HashMap artistMap, String filePath) throws IOException {
        LOGGER.info("Writing artist {} to file...", artist);

        File output = new File(filePath);
        FileWriter fw;
        BufferedWriter bw;

        fw = new FileWriter(output, true);
        bw = new BufferedWriter(fw);

        GsonBuilder gsonBuilder = new GsonBuilder().disableHtmlEscaping();
        Gson gson = gsonBuilder.setPrettyPrinting().create();
        HashMap<String, HashMap> finalMap = new HashMap<>();
        finalMap.put(artist, artistMap);
        String json = gson.toJson(finalMap);
        bw.write(json);
        bw.newLine();

        bw.close();
        fw.close();

        LOGGER.info("Writing concluded.");

        return (int) (output.length() / (1024 * 1024));
    }

}