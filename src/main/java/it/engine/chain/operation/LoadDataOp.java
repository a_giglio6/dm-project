package it.engine.chain.operation;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import it.common.chain.AbsOperation;
import it.common.chain.IScope;
import it.common.model.Album;
import it.common.model.Artist;
import it.common.model.Song;
import it.dao.mongo.CRUDMongoManager;
import org.apache.logging.log4j.LogManager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class LoadDataOp extends AbsOperation {

    private static final org.apache.logging.log4j.Logger LOGGER = LogManager.getLogger(LoadDataOp.class);

    @Override
    public void handleExec(IScope scope) throws Exception {

        HashMap hashMap = mapRebuild((String) scope.getParameter("filePath"));

        List<Artist> artists = getArtists(hashMap);

        loadArtistToDB(artists, scope);

    }

    public static void loadArtistToDB(List<Artist> artists, IScope scope) {
        CRUDMongoManager crudMongoManager = new CRUDMongoManager();

        LOGGER.info("Set up CRUDMongoManager...");
        crudMongoManager.setUpManager(scope);

        LOGGER.info("Writing Artists to DB...");
        crudMongoManager.write(artists);

        LOGGER.info("Closing CRUDMongoManager...");
        crudMongoManager.exitManager();
    }

    public static HashMap<String, HashMap> mapRebuild(String filePath) throws IOException {
        LOGGER.info("Building Map from file at \"{}\" ...", filePath);
        File input = new File(filePath);

        BufferedReader br = new BufferedReader(new FileReader(input));
        Gson gson = new Gson();
        HashMap hashMap = gson.fromJson(br, HashMap.class);
        br.close();

        LOGGER.info("Map built.");

        return  hashMap;
    }

    public static List<Artist> getArtists(HashMap hashMap){
        LOGGER.info("Creating Artist objects from map...");
        List<Artist> artists = new ArrayList<>();

        for (Object artistName : hashMap.keySet()){
            LOGGER.info("Creating Artist {}...", artistName);
            Artist artist = new Artist();
            artist.setName((String) artistName);

            LinkedTreeMap linkedTreeMap = (LinkedTreeMap) hashMap.get(artistName);
            HashMap artistMap = gsonMapToHashMap(linkedTreeMap);

            List<Album> albums = artist.getAlbums();
            for (Object albumName : artistMap.keySet()){
                LOGGER.info("Creating Album {}...", albumName);
                Album album = new Album();
                album.setName((String) albumName);

                LinkedTreeMap linkedTreeMap1 = (LinkedTreeMap) artistMap.get(albumName);
                HashMap albumMap = gsonMapToHashMap(linkedTreeMap1);

                List<Song> songs = album.getSongs();
                for (Object songName : albumMap.keySet()){
                    LOGGER.info("Creating Song {}...", songName);
                    Song song = new Song();
                    song.setName((String) songName);

                    LinkedTreeMap linkedTreeMap2 = (LinkedTreeMap) albumMap.get(songName);
                    HashMap songMap = gsonMapToHashMap(linkedTreeMap2);

                    String lyrics = (String) songMap.get("lyrics");
                    song.setLyrics(lyrics);

                    String date = (String) songMap.get("year");
                    song.setDate(date);

                    String genre = (String) songMap.get("genre");
                    song.setGenre(genre);

                    if (songs.isEmpty()){
                        album.setDate(date);
                        album.setGenre(genre);
                    }

                    songs.add(song);
                    LOGGER.info("Song added to list.");
                }
                albums.add(album);
                LOGGER.info("Album added to list.");
            }
            artists.add(artist);
            LOGGER.info("Artist added to list.");
        }

        return artists;

    }

    public static HashMap gsonMapToHashMap(LinkedTreeMap linkedTreeMap) {
        Gson gson = new Gson();

        String json = gson.toJson(linkedTreeMap);
        HashMap hashMap = gson.fromJson(json, HashMap.class);

        return hashMap;
    }

}
