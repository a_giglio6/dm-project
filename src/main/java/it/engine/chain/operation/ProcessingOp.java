package it.engine.chain.operation;

import com.google.common.base.Optional;
import com.google.gson.internal.LinkedTreeMap;
import com.opencsv.CSVReader;
import com.optimaize.langdetect.LanguageDetector;
import com.optimaize.langdetect.LanguageDetectorBuilder;
import com.optimaize.langdetect.i18n.LdLocale;
import com.optimaize.langdetect.ngram.NgramExtractors;
import com.optimaize.langdetect.profiles.LanguageProfile;
import com.optimaize.langdetect.profiles.LanguageProfileReader;
import com.optimaize.langdetect.text.CommonTextObjectFactories;
import com.optimaize.langdetect.text.TextObject;
import com.optimaize.langdetect.text.TextObjectFactory;
import it.common.chain.IScope;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import weka.core.Instances;
import weka.core.converters.CSVLoader;
import weka.core.tokenizers.NGramTokenizer;
import weka.filters.unsupervised.attribute.StringToWordVector;

import java.io.*;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static it.engine.chain.operation.LoadDataOp.*;

public class ProcessingOp implements it.common.chain.IOperation {

    private static final Logger LOGGER = LogManager.getLogger(ProcessingOp.class);

    @Override
    public void handleExec(IScope scope) throws Exception {

    }

    public static void getLyrics(HashMap hashMap){
        for (Object artist : hashMap.keySet()){
            LinkedTreeMap linkedTreeMap = (LinkedTreeMap) hashMap.get(artist);
            HashMap artistMap = gsonMapToHashMap(linkedTreeMap);
            for (Object album : artistMap.keySet()){
                LinkedTreeMap linkedTreeMap1 = (LinkedTreeMap) hashMap.get(album);
                HashMap albumMap = gsonMapToHashMap(linkedTreeMap1);
                for (Object song : albumMap.keySet()){
                    LinkedTreeMap linkedTreeMap2 = (LinkedTreeMap) hashMap.get(song);
                    HashMap songMap = gsonMapToHashMap(linkedTreeMap2);
                }
            }
        }
    }

    public static Instances arffConverter(String filePath) throws IOException {
        CSVLoader csvLoader = new CSVLoader();
        csvLoader.setSource(new File(filePath));

        return csvLoader.getDataSet();
    }

        public static String textCleaning(String lyrics){
        return lyrics == null ? null :
                Normalizer.normalize(lyrics, Normalizer.Form.NFD)
                        .replaceAll("\\p{InCombiningDiacriticalMarks}+", "")
                        .replaceAll("[^a-zA-Z0-9]", " ").toLowerCase();
    }
    
    public static StringToWordVector tokenization(Instances instances) throws Exception {
        NGramTokenizer nGramTokenizer = new NGramTokenizer();
        nGramTokenizer.setNGramMaxSize(2);
        StringToWordVector stringToWordVector = new StringToWordVector();
        stringToWordVector.setTokenizer(nGramTokenizer);
        stringToWordVector.setInputFormat(instances);

        return stringToWordVector;
    }



    public static boolean isEnglish(String lyrics) throws IOException {
        LOGGER.info("Detecting lyrics' language...");
        List<LanguageProfile> languageProfiles = new LanguageProfileReader().readAllBuiltIn();

        LanguageDetector languageDetector = LanguageDetectorBuilder.create(NgramExtractors.standard())
                .withProfiles(languageProfiles)
                .build();

        TextObjectFactory textObjectFactory = CommonTextObjectFactories.forDetectingOnLargeText();

        TextObject textObject = textObjectFactory.forText(lyrics);
        Optional<LdLocale> lang = languageDetector.detect(textObject);

        if (lang.isPresent()){
            LOGGER.info("Lyrics are in English.");
            return lang.get().getLanguage().equals("en");
        }
        else {
            LOGGER.info("Lyrics are not in English.");
            return false;
        }
    }

    public static void preprocess(String filePath, String targetPath) throws IOException {
        FileReader fileReader = new FileReader(filePath);
        CSVReader csvReader = new CSVReader(fileReader);

        String [] line;
        List<String> processedLines = new ArrayList<>();

        while ( (line = csvReader.readNext()) != null){
            String lyrics = line[1];
            if (isEnglish(lyrics)){
                line[1] = textCleaning(lyrics);
                processedLines.add(line[0]);
                processedLines.add(line[1]);
            }
        }
        processedLinesToCSV(processedLines, targetPath);

    }

    public static void processedLinesToCSV(List<String> list, String targetPath) throws IOException {
        FileWriter fileWriter = new FileWriter(targetPath);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

        try{
            for (String string : list){
                if (string.length() < 10){
                    bufferedWriter.write(string);
                    bufferedWriter.write(",\"");
                }
                else {
                    bufferedWriter.write(string);
                    bufferedWriter.write("\"");
                    bufferedWriter.newLine();
                }
            }
        } catch (Exception e){
            LOGGER.info("Exception occurred: {}", e.toString());
        } finally {
            bufferedWriter.close();
            fileWriter.close();
        }

    }

}
