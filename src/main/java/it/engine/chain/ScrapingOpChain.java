package it.engine.chain;

import it.common.chain.AbsOpChain;
import it.engine.chain.operation.LoadDataOp;
import it.engine.chain.operation.ScrapDataOp;
import it.engine.chain.operation.StartOp;

public class ScrapingOpChain extends AbsOpChain {

    @Override
    protected void populateChain() {

        chain.add(new ScrapDataOp());
        chain.add(new LoadDataOp());
    }
}
