package it.engine.chain;

import it.common.chain.AbsOpChain;
import it.engine.chain.operation.ClassificationOp;
import it.engine.chain.operation.SupervisedOp;

public class RecOpChain extends AbsOpChain {

    @Override
    protected void populateChain() {

        // chain.add(new RecoveryOp());
        chain.add(new SupervisedOp());
        chain.add(new ClassificationOp());
    }

}
