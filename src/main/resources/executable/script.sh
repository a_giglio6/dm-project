#!/bin/sh
echo 'Procedura per esame DB'

CP="./task2-3.jar"
MAIN="it.engine.cli.Runner"

DBNAME="Task2-3"
USER="root"
PASS="test"
DBCONN="mongodb+srv://FLapenna:Admin123@large-scale-task2-3-zjuvg.gcp.mongodb.net/test?retryWrites=true&w=majority"
NEO4JCONN="bolt://localhost:7687"
NEO4JUSER="neo4j"
NEO4JPASS="test"

java -Drelate.log.file=./../log -cp $CP $MAIN --dbname $DBNAME --user $USER --password $PASS --dbconn $DBCONN --neo4jConn $NEO4JCONN --neo4jUser $NEO4JUSER --neo4jPass $NEO4JPASS
