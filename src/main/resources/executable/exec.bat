:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: Procedura per esame DB
:: [OC], 11/10/2019
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

CLS

SET CP=./task2-3.jar
SET MAIN=it.engine.cli.Runner

SET DBNAME=Task2-3
SET USER=root
SET PASS=test
SET DBCONN=mongodb+srv://FLapenna:Admin123@large-scale-task2-3-zjuvg.gcp.mongodb.net/test?retryWrites=true&w=majority
SET NEO4JCONN=bolt://localhost:7687
SET NEO4JUSER=neo4j
SET NEO4JPASS=test

@ECHO Exec Application
java -Drelate.log.file=./../log -cp %CP% %MAIN% --dbname %DBNAME% --user %USER% --password %PASS% --dbconn %DBCONN% --neo4jConn %NEO4JCONN% --neo4jUser %NEO4JUSER% --neo4jPass %NEO4JPASS%

exit /b 0