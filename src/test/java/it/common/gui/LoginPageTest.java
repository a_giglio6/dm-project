package it.common.gui;

import it.common.chain.IScope;
import it.common.chain.Scope;
import it.common.model.Customer;
import it.dao.CRUDGenericManager;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class LoginPageTest {
    IScope scope = new Scope();

    @Before
    public void setUp() throws Exception {

        scope.addParameter(Scope.NEOCONN,"bolt://localhost:11002");
        scope.addParameter(Scope.NEOPASS,"test");
        scope.addParameter(Scope.NEOUSER,"neo4j");

        scope.addParameter(IScope.DBCONN,"mongodb://localhost:27017");
        scope.addParameter(IScope.DBNAME,"task2");

        CRUDGenericManager.setUpManager(scope);
    }

    @Test
    public void getClientTest(){
        LoginPage loginPage = new LoginPage(scope);
        Customer customer = (Customer) loginPage.getClient(scope,"lpn","123456789");
    }
}