package it.common.gui.user;

import it.common.chain.IScope;
import it.common.chain.Scope;
import it.common.model.*;
import it.dao.CRUDGenericManager;
import org.bson.types.ObjectId;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class HomePageClientTest {

    private IScope scope = new Scope();
    Song song = new Song();
    Song song2 = new Song();
    Customer customer = new Customer();
    Artist artist = new Artist();
    Artist artist2 = new Artist();
    Album album = new Album();
    Album album2 = new Album();

    @Before
    public void setUp() throws Exception {

        scope.addParameter(IScope.NEOCONN,"bolt://localhost:7687");
        scope.addParameter(IScope.NEOPASS,"test");
        scope.addParameter(IScope.NEOUSER,"neo4j");
        scope.addParameter(IScope.DBCONN,"mongodb://localhost:27017");
        scope.addParameter(IScope.DBNAME,"task2");

        CRUDGenericManager.setUpManager(scope);

        ObjectId x= new ObjectId();
        ObjectId y= new ObjectId();
        ObjectId z= new ObjectId();
        ObjectId w= new ObjectId();
        ObjectId i= new ObjectId();


        String name = "federico";
        String surname = "peluso";
        String address="via etnea";
        String city = "catania";
        String state = "Italy";
        String email = "gae@test.it";
        String prefgen= "pop";

        artist.setId(x);
        artist2.setId(y);
        artist.setName("BASSi Maestro");
        artist2.setName("BASSi Maestro");

        album.setName("Help");
        album.setArtist(artist2);
        album.setDate("2010");
        album.setGenre("pop");

        album2.setName("When we fall asleep?");
        album2.setArtist(artist2);
        album2.setDate("2010");
        album2.setGenre("pop");

        ObjectId objId2 = new ObjectId("5e1db97691be1540818581dc");
        song.setId(objId2);
        song.setAlbum(album2);
        song.setArtist(artist);
        song.setGenre("pop");
        song.setName("Bad Guy");
        song.setLyrics("ekjvbqòivbqòiubvòiqeubviubòeiuvbleubvlubvlre");

        ObjectId objId3 = new ObjectId("5e1c9479d716487f8de981f4");
        song2.setId(objId3);
        song2.setAlbum(album);
        song2.setArtist(artist2);
        song2.setGenre("rock");
        song2.setName("Yesterday");
        song2.setLyrics("ekjvbqòivbqòiubvòiqeubviubòeiuvbleubvlubvlre");

        List<Song> arlist = new ArrayList<>();
        arlist.add(song2);

        ObjectId objId = new ObjectId("5e1db97691be1540818581de");
        customer.setCustId(objId);
        customer.setPrefSong(arlist);
        customer.setFirstName(name);
        customer.setLastName(surname);
        customer.setAddress(address);
        customer.setCity(city);
        customer.setEmail(email);
        customer.setPrefGen("pop");
        customer.setState(state);

        scope = new Scope();
        scope.addParameter(IScope.CUSTOMER, customer);
    }
    @Test
    public void write(){
        CRUDGenericManager.createNeo4j(customer);
    }
    @Test
    public void testHomepage() throws InterruptedException {
        HomePageClient homePageClient = new HomePageClient(scope);
        TimeUnit.SECONDS.sleep(10000);
    }
}