package it.dao.mongo;

import it.common.chain.IScope;
import it.common.chain.Scope;
import it.common.model.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.types.ObjectId;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MongoTimeOperationTests {

    private Customer customer = new Customer();
    private Login login = new Login();
    private Artist artist = new Artist();
    private Artist artist2 = new Artist();
    private Album album = new Album();
    private Song song = new Song();
    CRUDMongoManager crudMongoManager = new CRUDMongoManager();
    IScope scope = new Scope();

    private static final Logger LOGGER = LogManager.getLogger(CRUDMongoManagerTest.class);

    @Before
    public void setUp() {

        scope.addParameter(IScope.DBCONN, "mongodb+srv://FLapenna:Admin123@large-scale-task2-3-zjuvg.gcp.mongodb.net/test?retryWrites=true&w=majority");
        scope.addParameter(IScope.DBNAME, "Task2-3");

        List<Album> albums = new ArrayList<>();
        List<Song> songs = new ArrayList<>();

        login.setUser("lpn");
        login.setPassword("123456789");
        customer.setAddress("via pippo");
        customer.setCity("Milano");
        customer.setEmail("pippo@gmail.com");
        customer.setFirstName("pippo");
        customer.setLastName("pluto");
        customer.setPrefGen("hiphop");
        customer.setState("IT");
        customer.setLogin(login);
        crudMongoManager.setUpManager(scope);

        artist.setName("BASSI Maestro");
        artist2.setName("BASSI Maestro21");

        album.setName("test1");
        album.setArtist(artist);
        album.setDate("2010");
        album.setGenre("hiphop");
        List<String> featart = new ArrayList<>();
        featart.add(artist2.getName());

        song.setAlbum(album);
        song.setArtist(artist);
        song.setGenre("hiphop");
        song.setName("pfnejh");
        song.setDate("2010");
        song.setFeat(Collections.singletonList(new ObjectId("5e2dcdbb62731c26bb3c2a29").toString()));
        song.setLyrics("plutto e tutto fayyo per la fattanza");


        songs.add(song);
        songs.add(song);
        album.setSongs(songs);
        albums.add(album);
        artist.setAlbums(albums);
        customer.setPrefSong(songs);

    }

    @Test
    public void readSongTest() {
        song.setName("constantly");
        Assert.assertNotNull("Read Song OK!", crudMongoManager.read(song));

    }

    @Test
    public void readArtistTest() {
        Assert.assertNotNull("Read Artist OK!", crudMongoManager.read(artist));

    }

    @Test
    public void mostFavoriteSongTest() {
        Assert.assertNotNull("Most Favorite Song OK!", crudMongoManager.getMostFavoriteSong());

    }

    @Test
    public void mostFavoriteSongForGenreTest() {
        Assert.assertNotNull("Most Favorite Song For Genre OK!", crudMongoManager.getMostFavoriteSongForGenre("Rock"));

    }

    @Test
    public void mostFavoriteSongForArtistTest() {
        artist.setId(new ObjectId("5e359f3d004cf175ba779f21"));
        Assert.assertNotNull("Most Favorite Song For Artist OK!", crudMongoManager.getMostFavoriteSongForArtist(artist));

    }

    @Test
    public void meanWordsForSongPerYearTest() {
        Assert.assertNotNull("Mean Words For Song Per Year OK!", crudMongoManager.getMeanWordsForSongPerYear());

    }

    @Test
    public void meanSongsForArtistPerYearTest() {
        Assert.assertNotNull("Mean Songs For Artist Per Year OK!", crudMongoManager.getMeanSongsForArtistPerYear());

    }

    @Test
    public void mostPreferGenreTest() {
        Assert.assertNotNull("Most Prefer Genre OK!", crudMongoManager.getMostPreferGenre());

    }

    @Test
    public void mostUsedWordsForArtistTest() {
        Assert.assertNotNull("Most Used Words For Artist OK!", crudMongoManager.getMostUsedWordsForArtist(artist));

    }

    @Test
    public void mostUsedWordsForGenreTest() {
        Assert.assertNotNull("Most Used Words For Genre OK!", crudMongoManager.getMostUsedWordsForGenre("Rock"));


    }

    @Test
    public void artistMostPrefTest() {
        Assert.assertNotNull("Artist Most Pref OK!", crudMongoManager.getArtistMostPref());
    }
}
