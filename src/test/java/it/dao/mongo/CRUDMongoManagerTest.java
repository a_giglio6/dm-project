package it.dao.mongo;

import it.common.chain.IScope;
import it.common.chain.Scope;
import it.common.model.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.types.ObjectId;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CRUDMongoManagerTest {

    private Customer customer = new Customer();
    private Login login = new Login();
    private Artist artist = new Artist();
    private Artist artist2 = new Artist();
    private Album album = new Album();
    private Song song = new Song();
    CRUDMongoManager crudMongoManager = new CRUDMongoManager();
    IScope scope = new Scope();

    private static final Logger LOGGER = LogManager.getLogger(CRUDMongoManagerTest.class);

    @Before
    public void setUp() {

        scope.addParameter(IScope.DBCONN, "mongodb+srv://FLapenna:Admin123@large-scale-task2-3-zjuvg.gcp.mongodb.net/test?retryWrites=true&w=majority");
        scope.addParameter(IScope.DBNAME, "Task2-3");

        List<Album> albums = new ArrayList<>();
        List<Song> songs = new ArrayList<>();

        login.setUser("lpn");
        login.setPassword("123456789");
        customer.setAddress("via pippo");
        customer.setCity("Milano");
        customer.setEmail("pippo@gmail.com");
        customer.setFirstName("pippo");
        customer.setLastName("pluto");
        customer.setPrefGen("hiphop");
        customer.setState("IT");
        customer.setLogin(login);
        crudMongoManager.setUpManager(scope);

        artist.setName("elaiza");
        artist2.setName("BASSI Maestro21");

        album.setName("test1");
        album.setArtist(artist);
        album.setDate("2010");
        album.setGenre("hiphop");
        List<String> featart = new ArrayList<>();
        featart.add(artist2.getName());

        song.setAlbum(album);
        song.setArtist(artist);
        song.setGenre("hiphop");
        song.setName("pfnejh");
        song.setDate("2010");
        song.setFeat(Collections.singletonList(new ObjectId("5e2dcdbb62731c26bb3c2a29").toString()));
        song.setLyrics("plutto e tutto fayyo per la fattanza");


        songs.add(song);
        songs.add(song);
        album.setSongs(songs);
        albums.add(album);
        artist.setAlbums(albums);
        customer.setPrefSong(songs);

    }

    @Test
    public void setUpManagerTest() {
        crudMongoManager.setUpManager(scope);
        Assert.assertNotNull(crudMongoManager.getMongoClient());

    }

    @Test
    public void readTest() {
        Assert.assertNotNull("Read Artist OK!", crudMongoManager.read(artist));
        Assert.assertNotNull("Read Customer OK!", crudMongoManager.read(customer));

    }

    @Test
    public void writeTest() {
        //Assert.assertTrue("Write Customer OK!",crudMongoManager.write(customer));
       // Assert.assertTrue("Write Artist OK!", crudMongoManager.write(artist));
       // List<Artist> artists = new ArrayList<>();
        Assert.assertTrue("Write Customer OK!", crudMongoManager.write(customer));
       /* artists.add(artist);
        artists.add(artist2);
        Assert.assertTrue("Write ListofArtist OK!",crudMongoManager.write(artists));*/


    }

    @Test
    public void defineAggTest() {
        artist.setId(new ObjectId("5e359f3d004cf175ba779f21"));
        Assert.assertNotNull("Most Favorite Song OK!", crudMongoManager.getMostFavoriteSong());
        Assert.assertNotNull("Most Favorite Song For Genre OK!", crudMongoManager.getMostFavoriteSongForGenre("hiphop"));
        Assert.assertNotNull("Most Favorite Song For Artist OK!", crudMongoManager.getMostFavoriteSongForArtist(artist));
        Assert.assertNotNull("Mean Words For Song Per Year OK!", crudMongoManager.getMeanWordsForSongPerYear());
        Assert.assertNotNull("Mean Songs For Artist Per Year OK!", crudMongoManager.getMeanSongsForArtistPerYear());
        Assert.assertNotNull("Most Prefer Genre OK!", crudMongoManager.getMostPreferGenre());
        Assert.assertNotNull("Most Used Words For Artist OK!", crudMongoManager.getMostUsedWordsForArtist(artist));
        Assert.assertNotNull("Most Used Words For Genre OK!", crudMongoManager.getMostUsedWordsForGenre("hiphop"));

        Assert.assertNotNull("Artist Most Pref OK!", crudMongoManager.getArtistMostPref());

    }

    @Test
    public void createIndexTest() {
        crudMongoManager.defineIndex();
    }

    @Test
    public void updateTest() {
    }

    @Test
    public void initTest(){
        crudMongoManager.initDB();
    }

    @Test
    public void deleteTest() {
    }

    @Test
    public void getRandomsSong(){
        customer.setPrefGen("Country");
        Assert.assertNotNull("Get random elements ok!",crudMongoManager.getRandomsSong(3,customer));
    }

    @Test
    public void getJsonTest() {
        String json = crudMongoManager.getJson(customer);
        Assert.assertNotNull("Json formato bene", json);
    }
}