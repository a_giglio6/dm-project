package it.dao;

import it.common.chain.IScope;
import it.common.chain.Scope;
import it.common.model.*;
import it.dao.neo4j.CRUDNeo4jManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.types.ObjectId;
import org.junit.Before;
import org.junit.Test;
import com.google.gson.Gson;
import org.neo4j.driver.internal.shaded.io.netty.util.collection.LongObjectHashMap;

import java.util.*;

public class CRUDGenericManagerTest {



    CRUDNeo4jManager crudNeo4jManager = new CRUDNeo4jManager();
    private Song song = new Song();
    private Song song2 = new Song();
    private Customer customer = new Customer();
    private Artist artist = new Artist();
    private Artist artist2 = new Artist();
    private Album album = new Album();
    private Album album2 = new Album();
    IScope scope = new Scope();

    @Before
    public void setUp() {

        scope.addParameter(Scope.NEOCONN,"bolt://localhost:11002");
        scope.addParameter(Scope.NEOPASS,"test");
        scope.addParameter(Scope.NEOUSER,"neo4j");


        CRUDGenericManager.setUpManager(scope);

        ObjectId x= new ObjectId();
        ObjectId y= new ObjectId();
        ObjectId z= new ObjectId();
        ObjectId w= new ObjectId();
        ObjectId i= new ObjectId();


        String name = "gaetano";
        String surname = "peluso";
        String address="via etnea";
        String city = "catania";
        String state = "Italy";
        String email = "gae@test.it";
        String prefgen= "pop";

        artist.setId(x);
        artist2.setId(y);
        artist.setName("BASSi Maestro");
        artist2.setName("ALTI Maestro21");

        album.setName("Black");
        album.setArtist(artist2);
        album.setDate("2010");
        album.setGenre("pop");

        album2.setName("");
        album2.setArtist(artist2);
        album2.setDate("2010");
        album2.setGenre("pop");

        ObjectId objId2 = new ObjectId("5e1c4e6e4b49dc438c433d54");
        song.setId(objId2);
        song.setAlbum(album);
        song.setArtist(artist);
        song.setGenre("pop");
        song.setName("RITMO");
        song.setLyrics("ekjvbqòivbqòiubvòiqeubviubòeiuvbleubvlubvlre");

        ObjectId objId3 = new ObjectId("5e14705d7d47b4412f88ad86");
        song2.setId(objId3);
        song2.setAlbum(album2);
        song2.setArtist(artist2);
        song2.setGenre("pop");
        song2.setName("andiamo a comandare");
        song2.setLyrics("vlkjenvejnvòebqvòvbheivb");

        List<Song> arlist = new ArrayList<Song>();
        arlist.add(song);


        ObjectId objId = new ObjectId("5e1c4e6e4b49dc438c433d56");
        customer.setCustId(objId);
        customer.setPrefSong(arlist);
        customer.setFirstName(name);
        customer.setLastName(surname);
        customer.setAddress(address);
        customer.setCity(city);
        customer.setEmail(email);
        customer.setPrefGen("pop");
        customer.setState(state);

    }

    @Test
    public void write() {
        boolean x = crudNeo4jManager.write(customer);
        crudNeo4jManager.close();
    }
    @Test
    public void read() {
        final Logger LOGGER = LogManager.getLogger(CRUDNeo4jManager.class);
        int size = customer.getPrefSong().size();
        Map<String, List<String>> alist = crudNeo4jManager.read(customer);
        for(int i=0; i<size; i++){
            for(Map.Entry<String, List<String>> entry : alist.entrySet()){
                String prefsong= customer.getPrefSong().get(i).getName();
                String son= entry.getValue().get(0); // song name
                String album_json = entry.getValue().get(1); //json object album
                String object= entry.getKey();// json object song
                Gson g = new Gson();
                Song p = g.fromJson(object, Song.class);
                if(!(prefsong.equalsIgnoreCase(son))) {
                    if (!customer.getPrefSong().contains(p)) {
                        //customer.getPrefSong().add(record);

                        Gson g2 = new Gson();
                        Album p2 = g2.fromJson(album_json, Album.class);
                        LOGGER.info(son);
                    }
                }
            }
        }
        //Assert.assertTrue("Write Customer OK!",crudNeo4jManager.write(customer));
        crudNeo4jManager.close();
    }


    @Test
    public void update(){
        crudNeo4jManager.update(song);

    }
    @Test
    public void deleteFavourite(){
        //crudNeo4jManager.deleteFavourite("174");

    }
}