package it.engine.chain.operation;

import org.junit.jupiter.api.Test;
import weka.core.Instances;
import weka.core.tokenizers.NGramTokenizer;
import weka.filters.unsupervised.attribute.StringToWordVector;

import java.io.IOException;
import java.util.List;

import static it.engine.chain.operation.ProcessingOp.*;
import static org.junit.jupiter.api.Assertions.*;
import static weka.core.tokenizers.Tokenizer.runTokenizer;

class ProcessingOpTest {

    public static final String FILE_PATH = "C:\\Users\\alegi\\Documents\\proj\\src\\main\\resources\\dataset.csv";
    public static final String TARGET_PATH = "C:\\Users\\alegi\\Documents\\proj\\src\\main\\resources\\proDataset.csv";

    private static String TEXT = "This software cannot handle it well when the input text is in none of the" +
            "expected (and supported) languages. For example if you only load the language profiles from English and" +
            "German, but the text is written in French, the program may pick the more likely one, or say it doesn't" +
            "know. (An improvement would be to clearly detect that it's unlikely one of the supported languages.";

    @Test
    void textCleaningTest() {
        String test = textCleaning("Wè ved!amo s& Funzìona?");
        System.out.println(test);
    }

    @Test
    void tokenizationTest() {
        String[] strings = new String[1];
        strings[0] = TEXT;
        NGramTokenizer nGramTokenizer = new NGramTokenizer();
        nGramTokenizer.setNGramMaxSize(1);
        runTokenizer(nGramTokenizer, strings);
    }

    @Test
    void dumbTest(){
        NGramTokenizer nGramTokenizer = new NGramTokenizer();
        nGramTokenizer.setNGramMaxSize(2);
        nGramTokenizer.tokenize(TEXT);
        System.out.println(TEXT);
    }

    @Test
    void arffConverterTest() throws IOException {
        Instances instances = arffConverter(FILE_PATH);
        System.out.println(instances.toString());
    }

    @Test
    void preprocessTest() throws IOException {
        preprocess(FILE_PATH, TARGET_PATH);
    }
}