package it.engine.chain.operation;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static it.engine.chain.operation.RecoveryOp.*;

class RecoveryOpTest {

    public static final String FILE_PATH = "C:\\Users\\alegi\\Desktop\\UNI\\Magistrale\\Data Mining\\Project\\Dataset-AllMusic-771Lyrics-xls.xls";
    public static final String CSV_PATH = "C:\\Users\\alegi\\Desktop\\UNI\\Magistrale\\Data Mining\\Project\\Dataset-AllMusic-771Lyrics-xls.csv";
    public static final String CSV_PATH1 = "C:\\Users\\alegi\\Desktop\\UNI\\Magistrale\\Data Mining\\Project\\Dataset-AllMusic-771Lyrics-xls1.csv";
    public static final String OUTPUT_PATH = "C:\\Users\\alegi\\Desktop\\UNI\\Magistrale\\Data Mining\\Project\\dataset.csv";


    @Test
    void lyricsRecoveryTest() throws InterruptedException, InvalidFormatException, IOException {
        lyricsRecovery(FILE_PATH);
    }

    @Test
    void songLyricsTest() throws InterruptedException {
        songLyrics("Genesis", "Uncertain Weather");
    }

    @Test
    void lyricsFilteringTest() throws InterruptedException, InvalidFormatException, IOException {
        lyricsFiltering(FILE_PATH);
    }

}