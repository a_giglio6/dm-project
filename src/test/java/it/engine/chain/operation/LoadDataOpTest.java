package it.engine.chain.operation;

import it.common.chain.IScope;
import it.common.chain.Scope;
import it.common.model.Artist;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import static it.engine.chain.operation.LoadDataOp.*;

class LoadDataOpTest {

    private static final String FILE_PATH = "/Users/alegi/Documents/task2-3/src/main/java/it/dbFile.txt";
    private static final String OUTPUT_PATH = "/Users/alegi/Documents/task2-3/src/main/java/it/dbFile.txt";

    IScope scope = new Scope();

    @BeforeEach
    public void setUp(){
        scope.addParameter("filePath", OUTPUT_PATH);
        scope.addParameter(IScope.DBCONN, "mongodb://localhost:27017");
        scope.addParameter(IScope.DBNAME, "task2");
    }

    @Test
    void handleExecTest() throws IOException {
        HashMap hashMap = mapRebuild((String) scope.getParameter("filePath"));
        List<Artist> artists = getArtists(hashMap);
        loadArtistToDB(artists, scope);
    }
}