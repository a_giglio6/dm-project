package it.engine.chain.operation;

import com.google.gson.internal.LinkedTreeMap;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Set;

import static it.engine.chain.operation.LoadDataOp.mapRebuild;
import static it.engine.chain.operation.ScrapDataOp.*;

class ScrapDataOpTest {

    private static final String LYRICS_URL = "https://www.lyrics.com/";
    private static final String OUTPUT_PATH = "/Users/alegi/Documents/task2-3/src/main/java/it/dbFile.txt";
    private static final String TEMP_PATH = "/Users/alegi/Documents/task2-3/src/main/java/it/tempFile.txt";
    private static final String GENRES_PATH = "/Users/alegi/Documents/task2-3/src/main/java/it/GenresList.txt";

    String url;
    Set songLinks;


    @BeforeEach
    void setUp() throws IOException, InterruptedException {
        url = LYRICS_URL + "/topartists.php";


    }

    @Test
    void getDiscogsSongDetailTest() throws IOException, InterruptedException {
        HashMap<String, String> test = new HashMap<>();
        getDiscogsSongDetail("Sicko mode", "Travis scott", "Astroworld", test);
    }

    @Test
    void testLyricsScraping() throws IOException, InterruptedException {
        HashMap<String, HashMap> map = lyricsScraping(OUTPUT_PATH);
        System.out.println(map.toString());
    }

    @Test
    void getAlbumMapTest() throws IOException, InterruptedException {
        songLinks = getLyricsSongs("https://www.lyrics.com/album/2290573");
        HashMap<String, HashMap> map = getAlbumMap(songLinks);
    }

    @Test
    void randomTest() throws IOException {
        try {
            Connection connection = Jsoup.connect("https://api.discogs.com/database/search?q={#PROUDCATOWNERREMIX%20XXXTentacion%20?%20}&key=TuIqieCbHmIhbLfBdCCo&secret=aduQISRJACmszKvWlSDmbyiGWPisaItG");
            Document doc = connection.get();
            System.out.println(connection.response().statusCode());
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    @Test
    void genreListTest() throws IOException {
        HashMap hashMap = mapRebuild(OUTPUT_PATH);
        Set genres = genreList(hashMap);

        File file = new File(GENRES_PATH);
        FileWriter fileWriter = new FileWriter(file);

        int i = 1;
        for (Object genre : genres){
            fileWriter.write((String) genre);
            if (i < genres.size()){
                fileWriter.write(",");
            }
            i++;
        }

        fileWriter.close();

    }

    @Test
    void fixFeaturingTest() throws IOException {
        HashMap hashMap = mapRebuild(OUTPUT_PATH);

        hashMap = fixFeaturing(hashMap);

        HashMap artistMap;

        for (Object artist : hashMap.keySet()){
            Object object = hashMap.get(artist);
            if (object.getClass().equals(LinkedTreeMap.class)){
                LinkedTreeMap linkedTreeMap = (LinkedTreeMap) hashMap.get(artist);
                artistMap = gsonMapToHashMap(linkedTreeMap);
            }
            else {
                artistMap = (HashMap) hashMap.get(artist);
            }
            artistToFile((String) artist, artistMap, TEMP_PATH);
            prettyFile(TEMP_PATH, OUTPUT_PATH);
        }
    }

}