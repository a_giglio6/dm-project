package it.engine.cli;

import org.junit.Test;

public class RunnerTest {

    @Test
    public void doTest() {

        String[] params = new String[] {
                "--dbconn", "mongodb+srv://FLapenna:Admin123@large-scale-task2-3-zjuvg.gcp.mongodb.net/test?retryWrites=true&w=majority",
                "--user","root",
                "--password","test",
                "--dbname","Task2-3",
                "--neo4jConn","bolt://localhost:7687",
                "--neo4jUser","neo4j",
                "--neo4jPass","test"
        };
        Runner.main(params);
    }

}
